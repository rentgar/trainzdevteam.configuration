﻿
namespace TrainzDevTeam.Configuration.Utilites
{
    
    /// <summary>
    /// Состояние блока данных файла конфигурации.
    /// </summary>
    public enum FragmentationBlockStatus
    {

        /// <summary>
        /// Свободный блок.
        /// </summary>
        Free,

        /// <summary>
        /// Блок не фрагментирован.
        /// </summary>
        NotScatterBlock,

        /// <summary>
        /// Блок фрагментирован.
        /// </summary>
        ScatterBlock,

    }

}
