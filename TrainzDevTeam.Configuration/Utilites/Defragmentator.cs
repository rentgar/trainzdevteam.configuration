﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace TrainzDevTeam.Configuration.Utilites
{
    /// <summary>
    /// Предоставляет механизм дефрагментации файла конфигурации.
    /// </summary>
    public sealed class Defragmentator : IDisposable
    {

        #region Объявление событий

        /// <summary>
        /// Возникает перед началом дефрагментации.
        /// </summary>
        public event EventHandler BeginDefragmentation;

        /// <summary>
        /// Возникает в процессе дефрагментации.
        /// </summary>
        public event EventHandler<DefragmentationEventArgs> DefragmentationStatusChanged;

        /// <summary>
        /// Возникает при окончании дефрагментации.
        /// </summary>
        public event EventHandler EndDefragmentation;

        #endregion

        #region Данные класса

        private FileStream _fstream;
        private object _syncroot;

        #endregion

        #region Конструктор

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="Defragmentator"/>.
        /// </summary>
        /// <param name="path">Абсолютный или относительный путь к файлу конфигурации.</param>
        /// <exception cref="ArgumentNullException">Аргумент <paramref name="path"/> не может быть null.</exception>
        /// <exception cref="ArgumentException"><paramref name="path"/> представляет собой пустую строку (""), содержащую только пробел или хотябы один недопустимый символ. -или- <paramref name="path"/>ссылается на устройство не файлового типа, например "con:", "com1:", "lpt1:" и т. д., в среде NTFS.</exception>
        /// <exception cref="System.NotSupportedException"><paramref name="path"/> ссылается на устройство не файлового типа, например "con:", "com1:", "lpt1:" и т. д., в среде, отличной от NTFS.</exception>
        /// <exception cref="FileNotFoundException">Не удается найти файл <paramref name="path"/>.</exception>
        /// <exception cref="IOException">Произошла ошибка ввода-вывода.</exception>
        /// <exception cref="DirectoryNotFoundException">Указан недопустимый путь (например, ведущий на несопоставленный диск).</exception>
        /// <exception cref="UnauthorizedAccessException">Файл доступен только для чтения.</exception>
        /// <exception cref="PathTooLongException">Указанный путь, имя файла или оба значения превышают максимальную длину, заданную в системе. Например, для платформ на основе Windows длина пути не должна превышать 248 знаков, а имен файлов — 260 знаков.</exception>
        /// <exception cref="WrongFileFormatException">Файл не является действительным файлом конфигурации.</exception>
        public Defragmentator(string path)
        {
            _fstream = new FileStream(path, FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            try {
                var filesignature = new byte[ConfigFile.signature.Length];
                if (_fstream.Read(filesignature, 0, filesignature.Length) != filesignature.Length) throw new WrongFileFormatException(path);
                if (!filesignature.SequenceEqual(ConfigFile.signature)) throw new WrongFileFormatException(path);
                _fstream.Seek(12, SeekOrigin.Begin);
                _fstream.Read(filesignature, 0, 2);
                BlocksCount = BitConverter.ToUInt16(filesignature, 0);
            } catch {
                _fstream.Dispose();
                throw;
            }
            _syncroot = new object();
        }

        #endregion

        #region Методы

        #region Внутренние

        private ushort ReadTableItem(ushort index)
        {
            var buffer = new byte[2];
            _fstream.Seek(14 + (index - 1) * 2, SeekOrigin.Begin);
            _fstream.Read(buffer, 0, buffer.Length);
            return BitConverter.ToUInt16(buffer, 0);
        }

        private bool GetConfigItemIsKey(ushort index)
        {
            _fstream.Seek(ConfigFile.BlockPosition(BlocksCount, index) + 59, SeekOrigin.Begin);
            return _fstream.ReadByte() == 0;
        }

        private ushort GetConfigItemFirstDataBlockIndex(ushort blockCount, ushort infoBlockIndex)
        {
            _fstream.Seek(ConfigFile.BlockPosition(blockCount, infoBlockIndex) + 62, SeekOrigin.Begin);
            var buffer = new byte[2];
            _fstream.Read(buffer, 0, buffer.Length);
            return BitConverter.ToUInt16(buffer, 0);
        }

        private string GetConfigItemName(ushort blockCount, ushort infoBlockIndex)
        {
            _fstream.Seek(ConfigFile.BlockPosition(blockCount, infoBlockIndex), SeekOrigin.Begin);
            var buffer = new byte[_fstream.ReadByte()];
            _fstream.Read(buffer, 0, buffer.Length);
            return (buffer.Length > 0 && buffer.Length <= 58) ? Encoding.GetEncoding(1251).GetString(buffer) : string.Empty;
        }

        private void SetConfigItemFirstDataBlockIndex(ushort blockCount, ushort infoBlockIndex, ushort value)
        {
            _fstream.Seek(ConfigFile.BlockPosition(blockCount, infoBlockIndex) + 62, SeekOrigin.Begin);
            _fstream.Write(BitConverter.GetBytes(value), 0, 2);
        }

        private ushort[] GetDataIndexList(ushort dataIndex)
        {
            var retlist = new List<ushort>();
            do {
                retlist.Add(dataIndex);
                dataIndex = ReadTableItem(dataIndex);
            } while (dataIndex != 0xffff);
            return retlist.ToArray();
        }


        #endregion

        #region Создание отчёта

        /// <summary>
        /// Получает отчёт о текущем состоянии фрагментации файла конфигурации.
        /// </summary>
        /// <returns>Объект <see cref="FragmentationOrder"/>, представляющий информацию о текущей фрагментации файла конфигурации.</returns>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего дефрагментатора были освобождены.</exception>
        public FragmentationOrder GetOrder()
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Defragmentator));
            lock (_syncroot) {
                var statusblocks = new FragmentationBlockStatus[BlocksCount];
                return new FragmentationOrder(BlocksCount, (ReadTableItem(0x0001) != 0x0000) ? GetKeyFragmentStatus(0x0001, ref statusblocks) : new BlockInfo(0, 0), statusblocks);
            }
        }

        private BlockInfo GetKeyFragmentStatus(ushort dataIndex, ref FragmentationBlockStatus[] table)
        {
            ushort occupiedblockcount = 0, notscatterblockcount = 0;
            var lastindex = dataIndex;
            do {
                if (!GetConfigItemIsKey(dataIndex)) {
                    var vdindex = GetConfigItemFirstDataBlockIndex(BlocksCount, dataIndex);
                    if (vdindex != 0x0000) {
                        var vlindex = vdindex;
                        do {
                            if (vlindex == vdindex || vdindex - 1 == vlindex) {
                                notscatterblockcount++;
                                table[vdindex - 1] = FragmentationBlockStatus.NotScatterBlock;
                            } else table[vdindex - 1] = FragmentationBlockStatus.ScatterBlock;
                            occupiedblockcount++;
                            vlindex = vdindex;
                            vdindex = ReadTableItem(vdindex);
                        } while (vdindex != 0xffff);
                    }
                } else {
                    var kdindex = GetConfigItemFirstDataBlockIndex(BlocksCount, dataIndex);
                    if (kdindex != 0x0000) {
                        var binfo = GetKeyFragmentStatus(kdindex, ref table);
                        occupiedblockcount += binfo.OccupiedBlock;
                        notscatterblockcount += binfo.NotScatterBlock;
                    }
                }
                occupiedblockcount++;
                if (lastindex == dataIndex || dataIndex - 1 == lastindex) {
                    notscatterblockcount++;
                    table[dataIndex - 1] = FragmentationBlockStatus.NotScatterBlock;
                } else table[dataIndex - 1] = FragmentationBlockStatus.ScatterBlock;
                lastindex = dataIndex;
                dataIndex = ReadTableItem(dataIndex);
            } while (dataIndex != 0xffff);
            return new BlockInfo(occupiedblockcount, notscatterblockcount);
        }

        #endregion

        #region Дефрагментация

        /// <summary>
        /// Выполняет дефрагментацию файла конфигцрации и уменьшает размер файла конфигурации, удаляя свободной пространство в области данных.
        /// </summary>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего дефрагментатора были освобождены.</exception>
        public void Defragmentation()
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Defragmentator));
            lock (_syncroot) {
                BeginDefragmentation?.Invoke(this, EventArgs.Empty);
                if (ReadTableItem(0x0001) != 0x0000) {
                    var @struct = SaveStructure(0x0001);
                    ushort startindex = 0x0001;
                    SortItems(@struct);
                    DefragIndexes(@struct, ref startindex);
                    var table = new ushort[BlocksCount];
                    var occupiedblockcount = GetOccupiedBlockCount(@struct);
                    startindex = 0x0000;
                    MoveData(@struct, @struct, ref occupiedblockcount, ref startindex);
                    CreateNewTable(ref table, @struct);
                    _fstream.Seek(14, SeekOrigin.Begin);
                    for (int i = 0; i < table.Length; ++i)
                        _fstream.Write(BitConverter.GetBytes(table[i]), 0, 2);
                    _fstream.SetLength(ConfigFile.BlockPosition(BlocksCount, (ushort)(occupiedblockcount + 1)));
                }
                EndDefragmentation?.Invoke(this, EventArgs.Empty);
            }
        }

        private List<IndexStructure> SaveStructure(ushort dataIndex)
        {
            var retlist = new List<IndexStructure>();
            if (dataIndex == 0x0000) return retlist;
            do {
                var name = GetConfigItemName(BlocksCount, dataIndex);
                var iskey = GetConfigItemIsKey(dataIndex);
                var kdindex = GetConfigItemFirstDataBlockIndex(BlocksCount, dataIndex);
                if (!iskey) {
                    ushort[] dataindexes = null;
                    if (kdindex != 0x0000) dataindexes = GetDataIndexList(kdindex);
                    retlist.Add(new IndexStructure(dataIndex, dataindexes, name));
                } else retlist.Add(new IndexStructure(dataIndex, name, SaveStructure(kdindex)));
                dataIndex = ReadTableItem(dataIndex);
            } while (dataIndex != 0xffff);
            return retlist;
        }

        private void SortItems(List<IndexStructure> items)
        {
            items.Sort();
            foreach (var item in items) {
                if (item.IsKey) SortItems(item.SubItems);
                else break;
            }
        }

        private void DefragIndexes(List<IndexStructure> items, ref ushort nextNewIndex)
        {
            foreach (var item in items)
                item.NewIndex = nextNewIndex++;
            foreach (var item in items) {
                if (!item.IsKey) {
                    var count = item.DataIndexCount;
                    for (int i = 0; i < count; ++i)
                        item.SetNewDataIndex((ushort)i, nextNewIndex++);
                } else DefragIndexes(item.SubItems, ref nextNewIndex);
            }
        }

        private ushort GetOccupiedBlockCount(List<IndexStructure> items)
        {
            ushort retcount = 0;
            foreach (var item in items) {
                if (item.IsKey) retcount += GetOccupiedBlockCount(item.SubItems);
                else retcount += item.DataIndexCount;
                retcount++;
            }
            return retcount;
        }

        private void MoveData(ushort sourceIndex, ushort destinationIndex, bool destinationBlockExist, ref ushort occupiedBlockCount, ref ushort blockIndex)
        {
            var sourcebuffer = new byte[64];
            var sourcestatus = FragmentationBlockStatus.Free;
            blockIndex++;
            _fstream.Seek(ConfigFile.BlockPosition(BlocksCount, sourceIndex), SeekOrigin.Begin);
            _fstream.Read(sourcebuffer, 0, sourcebuffer.Length);
            if (destinationBlockExist) {
                var destinationbuffer = new byte[64];
                _fstream.Seek(ConfigFile.BlockPosition(BlocksCount, destinationIndex), SeekOrigin.Begin);
                _fstream.Read(destinationbuffer, 0, destinationbuffer.Length);
                _fstream.Seek(ConfigFile.BlockPosition(BlocksCount, sourceIndex), SeekOrigin.Begin);
                _fstream.Write(destinationbuffer, 0, destinationbuffer.Length);
                sourcestatus = FragmentationBlockStatus.ScatterBlock;
            }
            _fstream.Seek(ConfigFile.BlockPosition(BlocksCount, destinationIndex), SeekOrigin.Begin);
            _fstream.Write(sourcebuffer, 0, sourcebuffer.Length);
            DefragmentationStatusChanged?.Invoke(this, new DefragmentationEventArgs(BlocksCount / (double)occupiedBlockCount, sourceIndex, destinationIndex, sourcestatus, FragmentationBlockStatus.NotScatterBlock));
        }

        private void MoveData(List<IndexStructure> @struct, List<IndexStructure> items, ref ushort occupiedBlockCount, ref ushort blockIndex)
        {
            foreach (var item in items) {
                var sdexist = ReadTableItem(item.NewIndex) != 0x0000;
                MoveData(item.Index, item.NewIndex, sdexist, ref occupiedBlockCount, ref blockIndex);
                if (sdexist) UpdateStructIndex(@struct, item.NewIndex, item.Index);
                item.Index = 0x0000;
                if (!item.IsKey) {
                    var count = item.DataIndexCount;
                    var newlist = item.NewDataIndexes;
                    for (int i = 0; i < count; ++i) {
                        var ddexist = ReadTableItem(newlist[i]) != 0x0000;
                        var oldindex = item.DataIndexes[i];
                        MoveData(oldindex, newlist[i], ddexist, ref occupiedBlockCount, ref blockIndex);
                        if (ddexist) UpdateStructIndex(@struct, newlist[i], oldindex);
                        item.ResetDataIndex((ushort)i);
                    }
                } else MoveData(@struct, item.SubItems, ref occupiedBlockCount, ref blockIndex);
                SetConfigItemFirstDataBlockIndex(BlocksCount, item.NewIndex, item.NewFirstDataIndex);
            }
        }

        private bool UpdateStructIndex(List<IndexStructure> @struct, ushort index, ushort newIndex)
        {
            foreach (var item in @struct) {
                if (item.Index == index) {
                    item.Index = newIndex;
                    return true;
                }
                if (!item.IsKey) {
                    var count = item.DataIndexCount;
                    for (int i = 0; i < count; ++i) {
                        if (item.DataIndexes[i] == index) {
                            item.UpdateDataIndex((ushort)i, newIndex);
                            return true;
                        }
                    }
                } else if (UpdateStructIndex(item.SubItems, index, newIndex)) return true;
            }
            return false;
        }

        private void CreateNewTable(ref ushort[] table, List<IndexStructure> items)
        {
            for (int i = 0; i < items.Count; ++i) {
                if (i == items.Count - 1) table[items[i].NewIndex - 1] = 0xffff;
                else table[items[i].NewIndex - 1] = items[i + 1].NewIndex;
                if (!items[i].IsKey) {
                    if (items[i].NewDataIndexCount != 0x0000) {
                        var indexes = items[i].NewDataIndexes;
                        for (int j = 0; j < indexes.Length; ++j) {
                            if (j == indexes.Length - 1) table[indexes[j] - 1] = 0xffff;
                            else table[indexes[j - 1]] = indexes[j + 1];
                        }
                    }
                } else CreateNewTable(ref table, items[i].SubItems);
            }
        }

        #endregion

        #region Дополнительно

        /// <summary>
        /// Освобождает все ресурсы, занятые текщим <see cref="Defragmentator"/>.
        /// </summary>
        public void Dispose()
        {
            if (!Disposed) {
                _fstream.Dispose();
                _fstream = null;
                Disposed = true;
            }
        }

        #endregion

        #endregion

        #region Свойства

        /// <summary>
        /// Получает значение указывающее что все ресурсы для текщуего дефрагментатора были освобождены.
        /// </summary>
        public bool Disposed { get; private set; }

        /// <summary>
        /// Получает количество блоков в файле конфигурации.
        /// </summary>
        public ushort BlocksCount { get; }

        #endregion

    }
}
