﻿
namespace TrainzDevTeam.Configuration.Utilites
{

    internal struct BlockInfo
    {

        #region Данные класса

        public readonly ushort OccupiedBlock;
        public readonly ushort NotScatterBlock;

        #endregion

        #region Конструктор

        public BlockInfo(ushort occupiedBlock, ushort notScatterBlock)
        {
            OccupiedBlock = occupiedBlock;
            NotScatterBlock = notScatterBlock;
        }

        #endregion

    }

}
