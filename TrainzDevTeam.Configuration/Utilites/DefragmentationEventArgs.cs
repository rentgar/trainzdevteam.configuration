﻿using System;

namespace TrainzDevTeam.Configuration.Utilites
{

    /// <summary>
    /// Представляет аргументы события дефрагментации
    /// </summary>
    public class DefragmentationEventArgs : EventArgs 
    {

        #region Данные класса

        private double _defragpercent;

        #endregion

        #region Конструктор

        internal DefragmentationEventArgs(double processPercent, ushort sourceIndex, ushort destinationIndex, FragmentationBlockStatus sourceBlockStatus, FragmentationBlockStatus destinationBlockStatus)
        {
            _defragpercent = processPercent;
            SourceMoveBlockIndex = sourceIndex;
            SourceMoveBlockStatus = sourceBlockStatus;
            DestinationMoveBlockIndex = destinationIndex;
            DestinationMoveBlockStatus = destinationBlockStatus;
        }

        #endregion

        #region Свойства

        /// <summary>
        /// Получает процент выполнения дефрагментации от 0 до 100.
        /// </summary>
        public int ProcessPercent => (int)(_defragpercent * 100);

        /// <summary>
        /// Возвращает одно из значение перечисления <see cref="FragmentationBlockStatus"/>, указывающее на состояние блока из которого выполнено копирование.
        /// </summary>
        public FragmentationBlockStatus SourceMoveBlockStatus { get; }

        /// <summary>
        /// Возвращает одно из значение перечисления <see cref="FragmentationBlockStatus"/>, указывающее на состояние блока в который выполняется копирование.
        /// </summary>
        public FragmentationBlockStatus DestinationMoveBlockStatus { get; }

        /// <summary>
        /// Получает индекс блока из которого выполнено копирование.
        /// </summary>
        public ushort SourceMoveBlockIndex { get; }

        /// <summary>
        /// Получает индекс блока в который вполнено копирование.
        /// </summary>
        public ushort DestinationMoveBlockIndex { get; }

        #endregion

    }

}
