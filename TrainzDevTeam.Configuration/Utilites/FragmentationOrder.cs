﻿using System.Collections.Generic;

namespace TrainzDevTeam.Configuration.Utilites
{
    /// <summary>
    /// Представляет отчёт о фрагментации файла конфигурации.
    /// </summary>
    public sealed class FragmentationOrder
    {

        #region Данные класса

        private BlockInfo _blockinfo;

        #endregion

        #region Конструктор

        internal FragmentationOrder(ushort blockCount, BlockInfo blockInfo, FragmentationBlockStatus[] statusBlocks)
        {
            BlocksCount = blockCount;
            _blockinfo = blockInfo;
            StatusBlocks = statusBlocks;
        }

        #endregion

        #region Свойства

        /// <summary>
        /// Получает количество блоков в файле конфигурации.
        /// </summary>
        public ushort BlocksCount { get; }

        /// <summary>
        /// Получает количество занятых блоков.
        /// </summary>
        public ushort OccupiedBlockCount => _blockinfo.OccupiedBlock;

        /// <summary>
        /// Получает количество свободных блоков.
        /// </summary>
        public ushort FreeBlockCount => (ushort)(BlocksCount - OccupiedBlockCount);

        /// <summary>
        /// Получает количество фрагментированных блоков.
        /// </summary>
        public ushort ScatterBlock => (ushort)(_blockinfo.OccupiedBlock - _blockinfo.NotScatterBlock);

        /// <summary>
        /// Получает значение от 0.0 до 1.0, представляющее проуент фрагментации файла конфигурации, где 0.0 - файл не фрагментирован, 1.0 - файл полностью фрагментирован.
        /// </summary>
        public double FragmentationPercent => ScatterBlock / (double)OccupiedBlockCount;

        /// <summary>
        /// Возвращает список значений <see cref="FragmentationBlockStatus"/>, представляющий состояние каждого блока файла конфигурации.
        /// </summary>
        /// <returns></returns>
        public IReadOnlyList<FragmentationBlockStatus> StatusBlocks { get; }

        #endregion

    }

}
