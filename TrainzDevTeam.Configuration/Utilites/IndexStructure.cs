﻿using System;
using System.Collections.Generic;

namespace TrainzDevTeam.Configuration.Utilites
{
    internal sealed class IndexStructure : IComparable<IndexStructure>
    {

        #region Конструктор

        internal IndexStructure(ushort index, string name, List<IndexStructure> subitems)
        {
            Index = index;
            Name = name;
            IsKey = true;
            SubItems = subitems;
        }

        internal IndexStructure(ushort index, ushort[] dataIndexes, string name)
        {
            Index = index;
            Name = name;
            IsKey = false;
            DataIndexes = dataIndexes;
            if (dataIndexes != null) NewDataIndexes = new ushort[dataIndexes.Length];
        }

        #endregion

        #region Методы

        public void UpdateDataIndex(ushort index, ushort newIndex) => DataIndexes[index] = newIndex;

        public void SetNewDataIndex(ushort index, ushort newIndex) => NewDataIndexes[index] = newIndex;

        public void ResetDataIndexes()
        {
            if (DataIndexes != null) {
                for (int i = 0; i < DataIndexes.Length; ++i)
                    DataIndexes[i] = 0;
            }
        }

        public void ResetDataIndex(ushort index) => DataIndexes[index] = 0;

        public int CompareTo(IndexStructure other)
        {
            var retvalue = (IsKey && !other.IsKey) ? 1 : (!IsKey && other.IsKey) ? -1 : 0;
            return (retvalue == 0) ? string.Compare(Name, other.Name, StringComparison.CurrentCultureIgnoreCase) : retvalue;
        }

        #endregion

        #region Свойства

        public ushort Index { get; set; }

        public ushort NewIndex { get; set; }

        public bool IsKey { get; }

        public string Name { get; }

        public List<IndexStructure> SubItems { get; }

        public ushort[] DataIndexes { get; }

        public ushort[] NewDataIndexes { get; }

        public ushort FirstDataIndex => (ushort)((IsKey && SubItems.Count > 0) ? SubItems[0].Index : (!IsKey && (DataIndexes?.Length ?? 0) != 0) ? DataIndexes[0] : 0);

        public ushort NewFirstDataIndex => (ushort)((IsKey && SubItems.Count > 0) ? SubItems[0].NewIndex : (!IsKey && (DataIndexes?.Length ?? 0) != 0) ? NewDataIndexes[0] : 0);

        public ushort DataIndexCount => (ushort)((IsKey) ? SubItems.Count : DataIndexes?.Length ?? 0);

        public ushort NewDataIndexCount => (ushort)((IsKey) ? SubItems.Count : NewDataIndexes?.Length ?? 0);

        #endregion

    }

}
