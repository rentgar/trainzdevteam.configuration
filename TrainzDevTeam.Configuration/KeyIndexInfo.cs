﻿using System;

namespace TrainzDevTeam.Configuration
{
    internal struct KeyIndexInfo : IEquatable<KeyIndexInfo> 
    {

        #region Данные класса

        internal readonly ushort ParentKeyIndex;
        internal readonly ushort ParentKeyDataIndex;
        internal readonly ushort KeyIndex;
        internal readonly ushort KeyDataIndex;

        #endregion

        #region Конструктор

        public KeyIndexInfo(ushort parentIndex, ushort parentDataIndex, ushort keyIndex, ushort keyDataIndex)
        {
            ParentKeyIndex = parentIndex;
            ParentKeyDataIndex = parentDataIndex;
            KeyIndex = keyIndex;
            KeyDataIndex = keyDataIndex;
        }

        #endregion

        #region Методы

        public bool Equals(KeyIndexInfo info) => ParentKeyIndex == info.ParentKeyIndex && ParentKeyDataIndex == info.ParentKeyDataIndex && KeyIndex == info.KeyIndex && KeyDataIndex == info.KeyDataIndex;

        public override bool Equals(object obj) => obj != null && obj is KeyIndexInfo && Equals((KeyIndexInfo)obj);

        public override int GetHashCode()
        {
            int parenthash = unchecked(ParentKeyIndex | ParentKeyDataIndex << 16);
            int keyhash = unchecked(KeyIndex | KeyDataIndex << 16);
            return unchecked(parenthash ^ keyhash);
        }

        #endregion

        #region Свойства

        public static KeyIndexInfo Empty => new KeyIndexInfo(0x0000, 0x0000, 0x0000, 0x0000);

        #endregion

        #region Операторы

        public static bool operator ==(KeyIndexInfo one, KeyIndexInfo two) => one.Equals(two);
        public static bool operator !=(KeyIndexInfo one, KeyIndexInfo two) => !one.Equals(two);

        #endregion

    }
}
