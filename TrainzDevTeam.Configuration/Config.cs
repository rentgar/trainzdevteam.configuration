﻿using System;
using System.IO;
using System.Drawing;
using System.Text.RegularExpressions;

[assembly: System.Resources.NeutralResourcesLanguage("en")]

namespace TrainzDevTeam.Configuration
{

    /// <summary>
    /// Представляет объект конфигурации
    /// </summary>
    public abstract class Config : IDisposable 
    {

        #region Перечисления

        internal enum DataType : byte
        {

            Key,
            Bytes,
            Byte,
            Int16,
            Int32,
            Int64,
            UInt16,
            UInt32,
            UInt64,
            Bool,
            Decimal,
            Double,
            SByte,
            Single,
            String,
            GUID,
            Point,
            Size,
            Version
        }

        #endregion

        #region Методы

        #region Статические

#pragma warning disable CA1062 

        public static PathValid CheckAbsoluteKeyPathValid(string path)
        {
            if (!AbsoluteKeyPathIsValid(path)) return PathValid.Invalid;
            return path.Trim('/').Split('/').Length > 32 ? PathValid.ExceedingOccurrenceDepth : PathValid.Valid;
        }

#pragma warning restore CA1062 

        /// <summary>
        /// Выполняет проверку, является ли указанный абсолютный путь к разделку конфигурации действительным.
        /// </summary>
        /// <param name="path">Полный путь к разделку конфигурации.</param>
        /// <returns>Значение true, если путь <paramref name="path"/> является действительным; в противном случае — значение false.</returns>
        public static bool AbsoluteKeyPathIsValid(string path) => path != null && Regex.IsMatch(path, @"(?<path>^(\/[A-z_0-9,.:{}А-я№@~\-""']{1}([A-z_0-9,.:{}А-я№@~\-""' ]{0,56}[A-z_0-9,.:{}А-я№@~\-""']{1})?)*)(?(path)(\/)?|(\/){1})$", RegexOptions.Singleline | RegexOptions.Compiled);

        /// <summary>
        /// Выполняет проверку, является ли указанный относительный путь к разделу конфигурации действительным.
        /// </summary>
        /// <param name="path">Относительный путь к разделу конфигурации.</param>
        /// <returns>Значение true, если путь <paramref name="path"/> является действительным; в противном случае — значение false.</returns>
        public static bool RelativeKeyPathIsValid(string path)=> path != null && Regex.IsMatch(path, @"^[A-z_0-9,.:{}А-я№@~\-""']{1}(?:[A-z_0-9,.:{}А-я№@~\-""' ]{0,56}[A-z_0-9,.:{}А-я№@~\-""']{1})?(?:\/[A-z_0-9,.:{}А-я№@~\-""']{1}(?:[A-z_0-9,.:{}А-я№@~\-""' ]{0,56}[A-z_0-9,.:{}А-я№@~\-""']{1})?)*\/?$", RegexOptions.Singleline | RegexOptions.Compiled);

        /// <summary>
        /// Выполняет проверку, является ли указангое имя раздела или параметра конфигурации действительным.
        /// </summary>
        /// <param name="name">Имя раздела или параметра конфигурации</param>
        /// <returns>Значение true, если имя <paramref name="name"/> является действительным; в противном случае — значение false.</returns>
        public static bool KeyOrValueNameIsValid(string name) => name != null && Regex.IsMatch(name, @"^[A-z_0-9,.:{}А-я№@~\-""']{1}([A-z_0-9,.:{}А-я№@~\-""' ]{0,56}[A-z_0-9,.:{}А-я№@~\-""']{1})?$", RegexOptions.Singleline | RegexOptions.Compiled);

        /// <summary>
        /// Создаёт новый файл конфигурации с указанным колличеством блоков и указанием возможности смены количества блоков.
        /// </summary>
        /// <param name="path">Абсолютный или относительный путь к файлу конфигурации.</param>
        /// <param name="blocksCount">Количество боков данных в файле (32-65534).</param>
        /// <param name="canChangeBlockCount">Указатель возможности изменения количества блоков данных в файле: значение true, если количество блоков может быть изменено; в противном случае — значение false.</param>
        /// <exception cref="ArgumentNullException">Параметр <paramref name="path"/> не может быть null.</exception>
        /// <exception cref="ArgumentException"><paramref name="path"/> представляет собой пустую строку (""), содержащую только пробел или хотябы один недопустимый символ. -или- <paramref name="path"/>ссылается на устройство не файлового типа, например "con:", "com1:", "lpt1:" и т. д., в среде NTFS.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Значение <paramref name="blocksCount"/> выходит за допустимые пределы (32-65534).</exception>
        /// <exception cref="System.NotSupportedException"><paramref name="path"/> ссылается на устройство не файлового типа, например "con:", "com1:", "lpt1:" и т. д., в среде, отличной от NTFS.</exception>
        /// <exception cref="FileNotFoundException">Не удается найти файл <paramref name="path"/>.</exception>
        /// <exception cref="IOException">Произошла ошибка ввода-вывода.</exception>
        /// <exception cref="DirectoryNotFoundException">Указан недопустимый путь (например, ведущий на несопоставленный диск).</exception>
        /// <exception cref="PathTooLongException">Указанный путь, имя файла или оба значения превышают максимальную длину, заданную в системе. Например, для платформ на основе Windows длина пути не должна превышать 248 знаков, а имен файлов — 260 знаков.</exception>
        public static void CreateConfigurationFile(string path, ushort blocksCount, bool canChangeBlockCount) => ConfigFile.CreateConfigFile(path, blocksCount, canChangeBlockCount).Dispose();

        /// <summary>
        /// Создаёт новый файл конфигурации с указанным колличеством блоков и без возможности смены их колличества.
        /// </summary>
        /// <param name="path">Абсолютный или относительный путь к файлу конфигурации.</param>
        /// <param name="blocksCount">Количество боков данных в файле (32-65534).</param>
        /// <exception cref="ArgumentNullException">Параметр <paramref name="path"/> не может быть null.</exception>
        /// <exception cref="ArgumentException"><paramref name="path"/> представляет собой пустую строку (""), содержащую только пробел или хотябы один недопустимый символ. -или- <paramref name="path"/>ссылается на устройство не файлового типа, например "con:", "com1:", "lpt1:" и т. д., в среде NTFS.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Значение <paramref name="blocksCount"/> выходит за допустимые пределы (32-65534).</exception>
        /// <exception cref="System.NotSupportedException"><paramref name="path"/> ссылается на устройство не файлового типа, например "con:", "com1:", "lpt1:" и т. д., в среде, отличной от NTFS.</exception>
        /// <exception cref="FileNotFoundException">Не удается найти файл <paramref name="path"/>.</exception>
        /// <exception cref="IOException">Произошла ошибка ввода-вывода.</exception>
        /// <exception cref="DirectoryNotFoundException">Указан недопустимый путь (например, ведущий на несопоставленный диск).</exception>
        /// <exception cref="PathTooLongException">Указанный путь, имя файла или оба значения превышают максимальную длину, заданную в системе. Например, для платформ на основе Windows длина пути не должна превышать 248 знаков, а имен файлов — 260 знаков.</exception>
        public static void CreateConfigurationFile(string path, ushort blocksCount) => ConfigFile.CreateConfigFile(path, blocksCount, false).Dispose();

        /// <summary>
        /// Создаёт новый файл конфигурации максимального размера и без возможности смены количества блоков.
        /// </summary>
        /// <param name="path">Абсолютный или относительный путь к файлу конфигурации.</param>
        /// <exception cref="ArgumentNullException">Параметр <paramref name="path"/> не может быть null.</exception>
        /// <exception cref="ArgumentException"><paramref name="path"/> представляет собой пустую строку (""), содержащую только пробел или хотябы один недопустимый символ. -или- <paramref name="path"/>ссылается на устройство не файлового типа, например "con:", "com1:", "lpt1:" и т. д., в среде NTFS.</exception>
        /// <exception cref="System.NotSupportedException"><paramref name="path"/> ссылается на устройство не файлового типа, например "con:", "com1:", "lpt1:" и т. д., в среде, отличной от NTFS.</exception>
        /// <exception cref="FileNotFoundException">Не удается найти файл <paramref name="path"/>.</exception>
        /// <exception cref="IOException">Произошла ошибка ввода-вывода.</exception>
        /// <exception cref="DirectoryNotFoundException">Указан недопустимый путь (например, ведущий на несопоставленный диск).</exception>
        /// <exception cref="PathTooLongException">Указанный путь, имя файла или оба значения превышают максимальную длину, заданную в системе. Например, для платформ на основе Windows длина пути не должна превышать 248 знаков, а имен файлов — 260 знаков.</exception>
        public static void CreateConfigurationFile(string path) => ConfigFile.CreateConfigFile(path, 65534, false).Dispose();

        /// <summary>
        /// Создаёт новый файл конфигурации с указанным колличеством блоков и с указанием возможности смены количества блоков и создаёт для него объект <see cref="Config"/>.
        /// </summary>
        /// <param name="path">Абсолютный или относительный путь к файлу конфигурации.</param>
        /// <param name="blocksCount">Количество боков данных в файле (32-65534).</param>
        /// <param name="canChangeBlockCount">Указатель возможности изменения количества блоков данных в файле: значение true, если количество блоков может быть изменено; в противном случае — значение false.</param>
        /// <returns>Объект <see cref="Config"/> представляющий доступ к управлению файлом конфигурации <paramref name="path"/>.</returns>
        /// <exception cref="ArgumentNullException">Параметр <paramref name="path"/> не может быть null.</exception>
        /// <exception cref="ArgumentException"><paramref name="path"/> представляет собой пустую строку (""), содержащую только пробел или хотябы один недопустимый символ. -или- <paramref name="path"/>ссылается на устройство не файлового типа, например "con:", "com1:", "lpt1:" и т. д., в среде NTFS.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Значение <paramref name="blocksCount"/> выходит за допустимые пределы (32-65534).</exception>
        /// <exception cref="System.NotSupportedException"><paramref name="path"/> ссылается на устройство не файлового типа, например "con:", "com1:", "lpt1:" и т. д., в среде, отличной от NTFS.</exception>
        /// <exception cref="FileNotFoundException">Не удается найти файл <paramref name="path"/>.</exception>
        /// <exception cref="IOException">Произошла ошибка ввода-вывода.</exception>
        /// <exception cref="DirectoryNotFoundException">Указан недопустимый путь (например, ведущий на несопоставленный диск).</exception>
        /// <exception cref="PathTooLongException">Указанный путь, имя файла или оба значения превышают максимальную длину, заданную в системе. Например, для платформ на основе Windows длина пути не должна превышать 248 знаков, а имен файлов — 260 знаков.</exception>
        public static Config CreateAndOpenConfigurationFile(string path, ushort blocksCount, bool canChangeBlockCount) => new ConfigFile(ConfigFile.CreateConfigFile(path, blocksCount, canChangeBlockCount));

        /// <summary>
        /// Создаёт новый файл конфигурации с указанным колличеством блоков и без возможности смены их колличества и создаёт для него объект <see cref="Config"/>.
        /// </summary>
        /// <param name="path">Абсолютный или относительный путь к файлу конфигурации.</param>
        /// <param name="blocksCount">Количество боков данных в файле (32-65534).</param>
        /// <returns>Объект <see cref="Config"/> представляющий доступ к управлению файлом конфигурации <paramref name="path"/>.</returns>
        /// <exception cref="ArgumentNullException">Параметр <paramref name="path"/> не может быть null.</exception>
        /// <exception cref="ArgumentException"><paramref name="path"/> представляет собой пустую строку (""), содержащую только пробел или хотябы один недопустимый символ. -или- <paramref name="path"/>ссылается на устройство не файлового типа, например "con:", "com1:", "lpt1:" и т. д., в среде NTFS.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Значение <paramref name="blocksCount"/> выходит за допустимые пределы (32-65534).</exception>
        /// <exception cref="System.NotSupportedException"><paramref name="path"/> ссылается на устройство не файлового типа, например "con:", "com1:", "lpt1:" и т. д., в среде, отличной от NTFS.</exception>
        /// <exception cref="FileNotFoundException">Не удается найти файл <paramref name="path"/>.</exception>
        /// <exception cref="IOException">Произошла ошибка ввода-вывода.</exception>
        /// <exception cref="DirectoryNotFoundException">Указан недопустимый путь (например, ведущий на несопоставленный диск).</exception>
        /// <exception cref="PathTooLongException">Указанный путь, имя файла или оба значения превышают максимальную длину, заданную в системе. Например, для платформ на основе Windows длина пути не должна превышать 248 знаков, а имен файлов — 260 знаков.</exception>
        public static Config CreateAndOpenConfigurationFile(string path, ushort blocksCount) => new ConfigFile(ConfigFile.CreateConfigFile(path, blocksCount, false));

        /// <summary>
        /// Создаёт новый файл конфигурации максимального размера и без возможности смены количества блоков и создаёт для него объект <see cref="Config"/>.
        /// </summary>
        /// <param name="path">Абсолютный или относительный путь к файлу конфигурации.</param>
        /// <returns>Объект <see cref="Config"/> представляющий доступ к управлению файлом конфигурации <paramref name="path"/>.</returns>
        /// <exception cref="ArgumentNullException">Параметр <paramref name="path"/> не может быть null.</exception>
        /// <exception cref="ArgumentException"><paramref name="path"/> представляет собой пустую строку (""), содержащую только пробел или хотябы один недопустимый символ. -или- <paramref name="path"/>ссылается на устройство не файлового типа, например "con:", "com1:", "lpt1:" и т. д., в среде NTFS.</exception>
        /// <exception cref="System.NotSupportedException"><paramref name="path"/> ссылается на устройство не файлового типа, например "con:", "com1:", "lpt1:" и т. д., в среде, отличной от NTFS.</exception>
        /// <exception cref="FileNotFoundException">Не удается найти файл <paramref name="path"/>.</exception>
        /// <exception cref="IOException">Произошла ошибка ввода-вывода.</exception>
        /// <exception cref="DirectoryNotFoundException">Указан недопустимый путь (например, ведущий на несопоставленный диск).</exception>
        /// <exception cref="PathTooLongException">Указанный путь, имя файла или оба значения превышают максимальную длину, заданную в системе. Например, для платформ на основе Windows длина пути не должна превышать 248 знаков, а имен файлов — 260 знаков.</exception>
        public static Config CreateAndOpenConfigurationFile(string path) => new ConfigFile(ConfigFile.CreateConfigFile(path, 65534, false));

        /// <summary>
        /// Открывает существующий файл конфигурации.
        /// </summary>
        /// <param name="path">Абсолютный или относительный путь к файлу конфигурации</param>
        /// <exception cref="ArgumentNullException">Параметр <paramref name="path"/> не может быть null.</exception>
        /// <exception cref="ArgumentException"><paramref name="path"/> представляет собой пустую строку (""), содержащую только пробел или хотябы один недопустимый символ. -или- <paramref name="path"/>ссылается на устройство не файлового типа, например "con:", "com1:", "lpt1:" и т. д., в среде NTFS.</exception>
        /// <exception cref="System.NotSupportedException"><paramref name="path"/> ссылается на устройство не файлового типа, например "con:", "com1:", "lpt1:" и т. д., в среде, отличной от NTFS.</exception>
        /// <exception cref="FileNotFoundException">Не удается найти файл <paramref name="path"/>.</exception>
        /// <exception cref="IOException">Произошла ошибка ввода-вывода.</exception>
        /// <exception cref="DirectoryNotFoundException">Указан недопустимый путь (например, ведущий на несопоставленный диск).</exception>
        /// <exception cref="PathTooLongException">Указанный путь, имя файла или оба значения превышают максимальную длину, заданную в системе. Например, для платформ на основе Windows длина пути не должна превышать 248 знаков, а имен файлов — 260 знаков.</exception>
        /// <exception cref="WrongFileFormatException">Файл не является действительным файлом конфигурации.</exception>
        public static Config OpenFromConfigurationFile(string path) => ConfigFile.FromFile(path);

        internal static string CombinePath(string absoluteKeyPath, string keyName)
        {
            var result = absoluteKeyPath;
            if (result.Substring(result.Length - 1, 1) != "/") result += "/";
            return result + keyName;
        }

        internal static string ValidPath(string path) => path != "/" ? path.TrimEnd('/') : path;

        #endregion

        #region Обслуживание

        /// <summary>
        /// Изменяет количество блоков конфигурации.
        /// </summary>
        /// <param name="newBlockCount">Новое количество блоков конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Количество блоков выходит за допустимые пределы.</exception>
        /// <exception cref="NotSupportedException">Текущая конфигурация не поддерживает смену количества вблоков.</exception>
        /// <exception cref="InvalidOperationException">Значение аргмента <paramref name="newBlockCount"/> не может быть меньше значения свойства <see cref="OccupiedBlocksCount"/>.</exception>
        public abstract void ChangeBlockCount(ushort newBlockCount);

        #endregion

        #region Разделы

        /// <summary>
        /// Создаёт новый раздел конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделку конфмигурации, в котором необходимо создать подраздел.</param>
        /// <param name="keyName">Имя нового раздела конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="keyName"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="keyName"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Глубина вложенности для создаваемого раздела конфигурации превосходит допустимое значение (Смотри так же <seealso cref="MaximumNestingDepth"/>).</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для создания раздела конфигурации (Смотри так же <seealso cref="FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="KeyAlreadyExistException">Раздел конфигурации с именем M<paramref name="keyName"/> уже существует в разделе <paramref name="path"/>.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public abstract void CreateKey(string path, string keyName);

        /// <summary>
        /// Переименовывает раздел конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделу конфигурации, который необходимо переименовать.</param>
        /// <param name="newKeyName">Новое наименование раздела конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="newKeyName"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="newKeyName"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="MaximumNestingDepth"/>).</exception>
        /// <exception cref="InvalidOperationException">Невозможно переименовать корневой раздел конфигурации.</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="KeyAlreadyExistException">Раздел конфигурации с именем M<paramref name="newKeyName"/> уже существует в базовом разделе для <paramref name="path"/>.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public abstract void RenameKey(string path, string newKeyName);

        /// <summary>
        /// Удаляет раздел конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделу конфигурации, который необходимо удалить.</param>
        /// <param name="recursive">Значение true, если нужно удалить все вложенные подразделы и параметры; в противном случае — значение false (будет выбрашено исключение <see cref="KeyNotEmptyException"/>, если удаляемый раздел не пустой).</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="MaximumNestingDepth"/>).</exception>
        /// <exception cref="InvalidOperationException">Невозможно удалить корневой раздел конфигурации.</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="KeyNotEmptyException">Удаляемый раздел конфигурации не пустой (только если аргумент <paramref name="recursive"/> имеет значение false).</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public abstract void DeleteKey(string path, bool recursive);

        /// <summary>
        /// Удаляет раздел конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделу конфигурации, который необходимо удалить.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="MaximumNestingDepth"/>).</exception>
        /// <exception cref="InvalidOperationException">Невозможно удалить корневой раздел конфигурации.</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="KeyNotEmptyException">Удаляемый раздел конфигурации не пустой.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public void DeleteKey(string path) => DeleteKey(path, false);

        /// <summary>
        /// Получает массив имён разделов конфигурации входящий в указанный раздел конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделу конфигурации для которого нужно получить список имён подразделов.</param>
        /// <returns>Массив <see cref="string"/>, содержащий список всех имён подразделов, указаного раздела конфигурации.</returns>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="MaximumNestingDepth"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public abstract string[] GetNameKeys(string path);

        /// <summary>
        /// Выполняет проверку наличия раздела конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделу конфигурации, наличие которого необходимо проверить.</param>
        /// <returns>Значение true, если указанный раздел конфигурации существует; в противном случае — значение false.</returns>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="MaximumNestingDepth"/>).</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public abstract bool ExistKey(string path);

        /// <summary>
        /// Создаёт объект <see cref="KeyInfo"/> для указаного раздела конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделу конфигурации.</param>
        /// <returns>Объект <see cref="KeyInfo"/> для указаного раздела конфигурации.</returns>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="MaximumNestingDepth"/>).</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public KeyInfo OpenKey(string path)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Config));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (!AbsoluteKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            var keynames = path.Trim('/').Split('/');
            if (keynames.Length > MaximumNestingDepth) throw new NestingDepthExceededException(ValidPath(path));
            return new KeyInfo(this, ValidPath(path));
        }

        /// <summary>
        /// Создаёт объект <see cref="KeyInfo"/> для указаного раздела конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделу конфигурации.</param>
        /// <param name="createIfNotExist">Значение true, если нужно создать раздел конфигурации в случае его отсутствия; в противном случае — значение false.</param>
        /// <returns>Объект <see cref="KeyInfo"/> для указаного раздела конфигурации.</returns>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="MaximumNestingDepth"/>).</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public KeyInfo OpenKey(string path, bool createIfNotExist)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Config));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (!AbsoluteKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            var keynames = path.Trim('/').Split('/');
            if (keynames.Length > MaximumNestingDepth) throw new NestingDepthExceededException(ValidPath(path));
            var keyinfo = new KeyInfo(this, ValidPath(path));
            if (createIfNotExist && !keyinfo.Exist) keyinfo.Create();
            return keyinfo;
        }

        #endregion

        #region Параметры

        /// <summary>
        /// Создаёт новый или заменят уже существующий параметр конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null. -или- Значение аргумента <paramref name="value"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="MaximumNestingDepth"/>).</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="TooLongDataException">Размер данных превоходит допустимое значение.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public abstract void SetValue(string path, string name, byte[] value);

        /// <summary>
        /// Создаёт новый или заменят уже существующий параметр конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="MaximumNestingDepth"/>).</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public abstract void SetValue(string path, string name, byte value);

        /// <summary>
        /// Создаёт новый или заменят уже существующий параметр конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="MaximumNestingDepth"/>).</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public abstract void SetValue(string path, string name, short value);

        /// <summary>
        /// Создаёт новый или заменят уже существующий параметр конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="MaximumNestingDepth"/>).</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public abstract void SetValue(string path, string name, int value);

        /// <summary>
        /// Создаёт новый или заменят уже существующий параметр конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="MaximumNestingDepth"/>).</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public abstract void SetValue(string path, string name, long value);

        /// <summary>
        /// Создаёт новый или заменят уже существующий параметр конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="MaximumNestingDepth"/>).</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public abstract void SetValue(string path, string name, ushort value);

        /// <summary>
        /// Создаёт новый или заменят уже существующий параметр конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="MaximumNestingDepth"/>).</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public abstract void SetValue(string path, string name, uint value);

        /// <summary>
        /// Создаёт новый или заменят уже существующий параметр конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="MaximumNestingDepth"/>).</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public abstract void SetValue(string path, string name, ulong value);

        /// <summary>
        /// Создаёт новый или заменят уже существующий параметр конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="MaximumNestingDepth"/>).</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public abstract void SetValue(string path, string name, bool value);

        /// <summary>
        /// Создаёт новый или заменят уже существующий параметр конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="MaximumNestingDepth"/>).</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public abstract void SetValue(string path, string name, decimal value);

        /// <summary>
        /// Создаёт новый или заменят уже существующий параметр конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="MaximumNestingDepth"/>).</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public abstract void SetValue(string path, string name, double value);

        /// <summary>
        /// Создаёт новый или заменят уже существующий параметр конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="MaximumNestingDepth"/>).</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public abstract void SetValue(string path, string name, sbyte value);

        /// <summary>
        /// Создаёт новый или заменят уже существующий параметр конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="MaximumNestingDepth"/>).</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public abstract void SetValue(string path, string name, float value);

        /// <summary>
        /// Создаёт новый или заменят уже существующий параметр конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null. -или- Значение аргумента <paramref name="value"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="MaximumNestingDepth"/>).</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="TooLongDataException">Размер данных превоходит допустимое значение.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public abstract void SetValue(string path, string name, string value);

        /// <summary>
        /// Создаёт новый или заменят уже существующий параметр конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="MaximumNestingDepth"/>).</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public abstract void SetValue(string path, string name, Guid value);

        /// <summary>
        /// Создаёт новый или заменят уже существующий параметр конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="MaximumNestingDepth"/>).</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public abstract void SetValue(string path, string name, Point value);

        /// <summary>
        /// Создаёт новый или заменят уже существующий параметр конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="MaximumNestingDepth"/>).</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public abstract void SetValue(string path, string name, Size value);

        /// <summary>
        /// Создаёт новый или заменят уже существующий параметр конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null. -или- Значение аргумента <paramref name="value"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="MaximumNestingDepth"/>).</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public abstract void SetValue(string path, string name, Version value);

        /// <summary>
        /// Получает массив имён параметров конфигурации входящий в указанный раздел конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделу конфигурации для которого нужно получить список имён параметров.</param>
        /// <returns>Массив <see cref="string"/>, содержащий список всех имён параметров, указаного раздела конфигурации.</returns>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="MaximumNestingDepth"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public abstract string[] GetNameValues(string path);

        /// <summary>
        /// Получает массив параметров конфигурации, входящих в указанный раздел конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделу конфигурации для которого нужно получить список параметров.</param>
        /// <returns>Массив <see cref="ValueInfo"/>, содержащий список всех параметров, указаного раздела конфигурации.</returns>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="MaximumNestingDepth"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public abstract ValueInfo[] GetInfoValues(string path);

        /// <summary>
        /// Переименовывает параметр конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="newName">Новое наименование параметра конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null. -или- Значение аргумента <paramref name="newName"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение. -или- Аргумент <paramref name="newName"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="MaximumNestingDepth"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="ValueNotFoundException">Раздел конфигурации <paramref name="name"/> не существует в разделе <paramref name="path"/>.</exception>
        /// <exception cref="ValueAlreadyExistException">Раздел конфигурации с именем <paramref name="newName"/> уже присутствует в разделе <paramref name="path"/>.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public abstract void RenameValue(string path, string name, string newName);

        /// <summary>
        /// Возвращает параметр конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <returns>Объект <see cref="ValueInfo"/>, описывающий параметр конфигурации.</returns>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="MaximumNestingDepth"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="ValueNotFoundException">Раздел конфигурации <paramref name="name"/> не существует в разделе <paramref name="path"/>.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public abstract ValueInfo GetValueInfo(string path, string name);

        /// <summary>
        /// Получает значение параметра конфигурации с указанием значения по умолчанию и необходимостью проверять тип параметра конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="defaultValue">Значение по умолчанию.</param>
        /// <param name="checkType">Значение true, если тип данных значение по умолчанию <paramref name="defaultValue"/> не будет совпадать с типом данных параметра, то будет возвращено значение по умолчанию <paramref name="defaultValue"/>; если значение false, значение по умолчанию <paramref name="defaultValue"/> будет возвращено только если параметр отсутствует.</param>
        /// <returns>Значение параметра или значение по умолчанию <paramref name="defaultValue"/> если параметр отсутствует или его тип не совпадает с ожидаемым при включённгой проверке типов <paramref name="checkType"/></returns>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null. -или- Значение аргумента <paramref name="defaultValue"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="MaximumNestingDepth"/>).</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public abstract object GetValue(string path, string name, object defaultValue, bool checkType);

        /// <summary>
        /// Получение значения параметра конфигурации с указанием значения по умолчанию и проверкой типа параметра конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="defaultValue">Значение по умолчанию.</param>
        /// <returns>Значение параметра или значение по умолчанию <paramref name="defaultValue"/> если параметр отсутствует или его тип не соответствует типу значения по умолчанию.</returns>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null. -или- Значение аргумента <paramref name="defaultValue"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="MaximumNestingDepth"/>).</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public abstract object GetValue(string path, string name, object defaultValue);

        /// <summary>
        /// Получение значения параметра.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <returns>Значение параметра конфигурации.</returns>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="MaximumNestingDepth"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="ValueNotFoundException">Раздел конфигурации <paramref name="name"/> не существует в разделе <paramref name="path"/>.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public abstract object GetValue(string path, string name);

        /// <summary>
        /// Получение значения параметра как строки.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="defaultValue">Значение по умолчанию.</param>
        /// <param name="canConvertType">Значение true, если нужно преобразовать значение другого типа к строке; значение false — будет возвращено значение по умолчанию <paramref name="defaultValue"/>, для несоответствующего типа параметра.</param>
        /// <returns>Значение параметра или значение по умолчанию <paramref name="defaultValue"/> если прараметр отсутствует или имеет не строковый тип с отключённой конвертацией <paramref name="canConvertType"/></returns>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="MaximumNestingDepth"/>).</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public abstract string GetValueAsString(string path, string name, string defaultValue = null, bool canConvertType = true);

        /// <summary>
        /// Получение значения параметра как логическое знчаение.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="defaultValue">Значение по умолчанию.</param>
        /// <param name="canConvertType">Значение true, если нужно преобразовать значение другого типа к строке; значение false — будет возвращено значение по умолчанию <paramref name="defaultValue"/>, для несоответствующего типа параметра.</param>
        /// <returns>Значение параметра или значение по умолчанию <paramref name="defaultValue"/> если прараметр отсутствует или имеет не строковый тип с отключённой конвертацией <paramref name="canConvertType"/></returns>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="MaximumNestingDepth"/>).</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public abstract bool GetValueAsBool(string path, string name, bool defaultValue = false, bool canConvertType = true);

        /// <summary>
        /// Получение значения параметра как <see cref="Guid"/>.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="defaultValue">Значение по умолчанию.</param>
        /// <param name="canConvertType">Значение true, если нужно преобразовать значение другого типа к строке; значение false — будет возвращено значение по умолчанию <paramref name="defaultValue"/>, для несоответствующего типа параметра.</param>
        /// <returns>Значение параметра или значение по умолчанию <paramref name="defaultValue"/> если прараметр отсутствует или имеет не строковый тип с отключённой конвертацией <paramref name="canConvertType"/></returns>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="MaximumNestingDepth"/>).</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public abstract Guid GetValueAsGuid(string path, string name, Guid defaultValue = default, bool canConvertType = true);

        /// <summary>
        /// Получение значения параметра как <see cref="Version"/>
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="defaultValue">Значение по умолчанию</param>
        /// <param name="canConvertType">Значение true, если нужно преобразовать значение другого типа к строке; значение false — будет возвращено значение по умолчанию <paramref name="defaultValue"/>, для несоответствующего типа параметра.</param>
        /// <returns>Значение параметра или значение по умолчанию <paramref name="defaultValue"/> если прараметр отсутствует или имеет не строковый тип с отключённой конвертацией <paramref name="canConvertType"/></returns>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="MaximumNestingDepth"/>).</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public abstract Version GetValueAsVersion(string path, string name, Version defaultValue = null, bool canConvertType = true);

        /// <summary>
        /// Получение значения параметра конфигурации с указанным типом значения.
        /// </summary>
        /// <typeparam name="TValue">Тип возвращаемого значения.</typeparam>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="defaultValue">Значение по умолчанию.</param>
        /// <returns>Значение параметра или значение по умолчанию <paramref name="defaultValue"/> если параметр отсутствует или его тип не соответствует типу значения по умолчанию.</returns>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="MaximumNestingDepth"/>).</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public abstract TValue GetValueAs<TValue>(string path, string name, TValue defaultValue = default);

        /// <summary>
        /// Удаление параметра конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="MaximumNestingDepth"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="ValueNotFoundException">Раздел конфигурации <paramref name="name"/> не существует в разделе <paramref name="path"/>.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public abstract void DeleteValue(string path, string name);

        /// <summary>
        /// Выполняет проверку наличия параметра конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <returns>Значение true, если параметр конфигурации <paramref name="name"/> существует в разделе конфигурации <paramref name="path"/>; в противном случае — значение false.</returns>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="MaximumNestingDepth"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public abstract bool ExistValue(string path, string name);

        #endregion

        #region Операторы

        /// <summary>
        /// Создаёт объект <see cref="KeyInfo"/> для указаного раздела конфигурации.
        /// </summary>
        /// <param name="configuration">Конфигурация, для которой нужно открыть раздел конфигурации.</param>
        /// <param name="path">Путь к разделу конфигурации.</param>
        /// <returns>Объект <see cref="KeyInfo"/> для указаного раздела конфигурации.</returns>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="MaximumNestingDepth"/>).</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Usage", "CA2225", Justification = "<Ожидание>")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1062", Justification = "<Ожидание>")]
        public static KeyInfo operator +(Config configuration, string path) => configuration.OpenKey(path);

        #endregion

        #region Дополнительно

        /// <summary>
        /// Освобождает все ресурсы знаятные объектом <see cref="Config"/>
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1063", Justification = "<Ожидание>")]
        public void Dispose()
        {
            if(CanDisposed && !Disposed) {
                Dispose(true);
                Disposed = true;
                GC.SuppressFinalize(this);
            }
        }

        /// <summary>
        /// Исключает текущую конфигурацию из реестра конфигурации, указанного в свойстве <see cref="Registry"/>, и вызывает для неё метод <see cref="Dispose()"/>
        /// </summary>
        /// <param name="leaveOpen">Значение true, если нужно оставить конфигурацию открытой (не вызывать <see cref="Dispose()"/>); в противном случае — значение flase.</param>
        /// <returns>Значение true, если конфигурация была исключена; в противном случае — значение false.</returns>
        public bool ExclusionFromRegistry(bool leaveOpen)
        {
            if (Registry != null) {
                Registry.ExclusionFile(Registry.GetRegistryKeyName(this), leaveOpen);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Исключает текущую конфигурацию из реестра конфигурации, указанного в свойстве <see cref="Registry"/>, и вызывает для неё метод <see cref="Dispose()"/>
        /// </summary>
        /// <returns>Значение true, если конфигурация была исключена; в противном случае — значение false.</returns>
        public bool ExclusionFromRegistry() => ExclusionFromRegistry(false);

        /// <summary>
        /// Исключает конфигурации из реестра и освобождает все ресурсы связанные с ней.
        /// </summary>
        public void Close()
        {
            ExclusionFromRegistry();
            Dispose();
        }

        protected abstract void Dispose(bool disposed);

        #endregion

        #endregion

        #region Свойства

        /// <summary>
        /// Возаращает значение true, если все ресурсы для этого объекта конфигурации были освобождены; в противном случае — значение false.
        /// </summary>
        public bool Disposed { get; private set; }

        /// <summary>
        /// Возвращает объект <see cref="Configuration.Registry"/>, представляющий регистр конфигурации, которому принадлежит данная конфигурация, или значение null, если конфигурация не относится к реестру.
        /// </summary>
        public Registry Registry { get; internal set; }

        /// <summary>
        /// Возвращает значение true, если ресурсы текущего объекта <see cref="Config"/> могут быть освобождены вызовом функции <see cref="Dispose()"/>; в противном случае — значение false.
        /// </summary>
        public bool CanDisposed => Registry == null;

        /// <summary>
        /// Возвращает значение true, если допускается изменения количества блоков конфигурации; в противном случае — значение false.
        /// </summary>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        public abstract bool CanChangeBlocksCount { get; }

        /// <summary>
        /// Возвращает количество блоков конфигурации.
        /// </summary>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        public abstract int BlocksCount { get; }

        /// <summary>
        /// Возвращает количество свободных блоков конфигурации.
        /// </summary>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        public abstract int FreeBlocksCount { get; }

        /// <summary>
        /// Возвращает количество занятых блоков конфигурации.
        /// </summary>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        public abstract int OccupiedBlocksCount { get; }

        /// <summary>
        /// Возвращает полный путь к файцлу конфигурации, связаного с данный конфигурацией.
        /// </summary>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        public abstract string FileName { get; }

        /// <summary>
        /// Возвращает значение макисмальной глубины вложенности для разделов конфигурации.
        /// </summary>
        public abstract int MaximumNestingDepth { get; }

        #endregion

    }

}
