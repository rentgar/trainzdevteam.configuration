﻿using System;

namespace TrainzDevTeam.Configuration
{

    internal struct IndexInfo : IEquatable<IndexInfo> 
    {

        #region Данные класса

        public readonly ushort PreviousIndex;
        public readonly ushort Index;

        #endregion

        #region Конструктор

        public IndexInfo(ushort previousIndex, ushort index)
        {
            PreviousIndex = previousIndex;
            Index = index;
        }

        #endregion

        #region Методы

        public bool Equals(IndexInfo info) => info.PreviousIndex == PreviousIndex && info.Index == Index;

        public override bool Equals(object obj) => obj != null && obj is IndexInfo && Equals((IndexInfo)obj);

        public override int GetHashCode() => unchecked(PreviousIndex << 16 | Index);

        #endregion

        #region Свойства

        public static IndexInfo Empty => new IndexInfo(0, 0);

        #endregion

        #region Операторы

        public static bool operator ==(IndexInfo a, IndexInfo b) => a.Equals(b);

        public static bool operator !=(IndexInfo a, IndexInfo b) => !a.Equals(b);

        #endregion

    }

}
