﻿
namespace TrainzDevTeam.Configuration
{

    /// <summary>
    /// Определяет состояние допустимости пути к элементу конфигурации.
    /// </summary>
    public enum PathValid
    {

        /// <summary>
        /// Недопустимый путь.
        /// </summary>
        Invalid,

        /// <summary>
        /// Путь превосходит глубину вложености.
        /// </summary>
        ExceedingOccurrenceDepth,

        /// <summary>
        /// Допустимый путь.
        /// </summary>
        Valid,

    }
}
