﻿using System;
using System.Globalization;

namespace TrainzDevTeam.Configuration
{

    /// <summary>
    /// Исключенеи, выбрасываемое, если файл конфигурации имеет неверный формат
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1032")]
    public class WrongFileFormatException : Exception 
    {

        #region Конструктор

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="WrongFileFormatException"/>
        /// </summary>
        /// <param name="path">Полный путь к файлу конфигурации с которым связано исключение</param>
        public WrongFileFormatException(string path) : base(string.Format(CultureInfo.CurrentCulture, ResourceStrings.Exception_WrongFileFormat, path)) => FilePath = path;

        #endregion

        #region Свойства

        /// <summary>
        /// Возвращает путь к файлу, с которым связано исключение
        /// </summary>
        public string FilePath { get; }


        #endregion

    }
}
