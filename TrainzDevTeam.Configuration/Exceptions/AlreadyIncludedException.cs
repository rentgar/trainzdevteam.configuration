﻿using System;
using System.Globalization;

namespace TrainzDevTeam.Configuration
{

    /// <summary>
    /// Исключение, выбрасываемое при попытке добавить объект реестра, который уже присутствует.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1032")]
    public class AlreadyIncludedException : Exception 
    {

        #region Конструктор

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="AlreadyIncludedException"/>
        /// </summary>
        /// <param name="filename">Имя файла конфигурации, связаного с исключением</param>
        public AlreadyIncludedException(string filename) : base(string.Format(CultureInfo.CurrentCulture, ResourceStrings.Exception_ConfigFileIncludedRegistry, filename)) => FileName = filename;

        #endregion

        #region Свойства

        /// <summary>
        /// Получает имя файла, с которым связано исключение
        /// </summary>
        public string FileName { get; }

        #endregion

    }

}
