﻿using System;

namespace TrainzDevTeam.Configuration
{

    /// <summary>
    /// Исключение, выбрасываемое в случае отсутствия свободного места в конфигураци
    /// </summary>
    public class NotEnoughFreeSpaceException : Exception 
    {

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="NotEnoughFreeSpaceException"/>.
        /// </summary>
        /// <param name="message">Сообщение, связаное с исключением.</param>
        public NotEnoughFreeSpaceException(string message) : base(message) { }

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="NotEnoughFreeSpaceException"/>.
        /// </summary>
        /// <param name="message">Сообщение, связаное с исключением.</param>
        /// <param name="innerException">Исключение, вызвавшее текущее исключение, или значение null, если внутреннее исключение не задано.</param>
        public NotEnoughFreeSpaceException(string message, Exception innerException) : base(message, innerException) { }

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="NotEnoughFreeSpaceException"/> с сообщением по умолчанию.
        /// </summary>
        public NotEnoughFreeSpaceException():base(ResourceStrings.Exception_NotEnoughFreeSpace) { }

    }

}
