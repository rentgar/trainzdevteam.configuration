﻿using System;

namespace TrainzDevTeam.Configuration
{

    /// <summary>
    /// Исключение, выбрасываемое в случае, если размер данных превышает допустимые значения
    /// </summary>
    public class TooLongDataException : Exception 
    {

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="TooLongDataException"/>.
        /// </summary>
        /// <param name="message">Сообщение, связаное с исключением.</param>
        public TooLongDataException(string message) : base(message) { }

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="TooLongDataException"/>.
        /// </summary>
        /// <param name="message">Сообщение, связаное с исключением.</param>
        /// <param name="innerException">Исключение, вызвавшее текущее исключение, или значение null, если внутреннее исключение не задано.</param>
        public TooLongDataException(string message, Exception innerException) : base(message, innerException) { }

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="TooLongDataException"/> с сообщением по умолчанию.
        /// </summary>
        public TooLongDataException() : base(ResourceStrings.Exception_TooLongData) { }

    }
}
