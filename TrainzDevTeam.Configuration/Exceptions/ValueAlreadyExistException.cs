﻿using System;
using System.Globalization;

namespace TrainzDevTeam.Configuration
{

    /// <summary>
    /// Исключение, выбрасываемое в случае существования параметра конфигурации.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1032")]
    public class ValueAlreadyExistException : Exception 
    {

        #region Конструктор

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="ValueAlreadyExistException"/>.
        /// </summary>
        /// <param name="keyPath">Путь к разделу конфигурации, связанному с исключением.</param>
        /// <param name="valueName">Наименование параметра конфигурации, связанного с исключениемю.</param>
        public ValueAlreadyExistException(string keyPath, string valueName) : base(string.Format(CultureInfo.CurrentCulture, ResourceStrings.Exception_ValueAlreadyExist, keyPath, valueName))
        {
            KeyPath = keyPath;
            ValueName = valueName;
        }

        #endregion

        #region Свойства

        /// <summary>
        /// Возвращает путь к разделу конфигурации, с которым связано событие.
        /// </summary>
        public string KeyPath { get; }

        /// <summary>
        /// Возвращает наименование параметра, с которым связано исключение.
        /// </summary>
        public string ValueName { get; }

        #endregion

    }

}
