﻿using System;
using System.Globalization;

namespace TrainzDevTeam.Configuration
{

    /// <summary>
    /// Исключение, выбрасываемое при отсутствиии целевого параметра конфигурации.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1032")]
    public class ValueNotFoundException : Exception 
    {

        #region Конструктор

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="ValueNotFoundException"/>.
        /// </summary>
        /// <param name="keyPath">Путь к разделу конфигурации, связанному с исключением.</param>
        /// <param name="valueName">Наименование параметра конфигурации, связанного с исключениемю.</param>
        public ValueNotFoundException(string keyPath, string valueName) : base(string.Format(CultureInfo.CurrentCulture, ResourceStrings.Exception_ValueNotFound, keyPath, valueName))
        {
            KeyPath = keyPath;
            ValueName = valueName;
        }

        #endregion

        #region Свойства

        /// <summary>
        /// Возвращает путь к разделу конфигурации, с которым связано событие.
        /// </summary>
        public string KeyPath { get; }

        /// <summary>
        /// Возвращает наименование параметра, с которым связано исключение.
        /// </summary>
        public string ValueName { get; }

        #endregion


    }
}
