﻿using System;
using System.Globalization;

namespace TrainzDevTeam.Configuration
{

    /// <summary>
    /// Исключение, выбрасываемое в случае превышения глубины вложености для разделов конфигурации.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1032")]
    public class NestingDepthExceededException : Exception 
    {

        #region Конструктор

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="NestingDepthExceededException"/>.
        /// </summary>
        /// <param name="keyPath">Путь к разделу конфигурации, связанному с исключением.</param>
        public NestingDepthExceededException(string keyPath) : base(string.Format(CultureInfo.CurrentCulture, ResourceStrings.Exception_KeyNotFound, keyPath)) => KeyPath = keyPath;

        #endregion

        #region Свойства

        /// <summary>
        /// Возвращает путь к разделу конфигурации, с которым связано событие.
        /// </summary>
        public string KeyPath { get; }

        #endregion

    }

}
