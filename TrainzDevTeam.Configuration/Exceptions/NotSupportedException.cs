﻿using System;

namespace TrainzDevTeam.Configuration
{

    /// <summary>
    /// Исключение, выбрасываемое при попытке выполнить недопустимую операцию.
    /// </summary>
    public class NotSupportedException : InvalidOperationException 
    {

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="NotSupportedException"/>.
        /// </summary>
        /// <param name="message">Сообщение, связаное с исключением.</param>
        public NotSupportedException(string message) : base(message) { }

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="NotSupportedException"/>.
        /// </summary>
        /// <param name="message">Сообщение, связаное с исключением.</param>
        /// <param name="innerException">Исключение, вызвавшее текущее исключение, или значение null, если внутреннее исключение не задано.</param>
        public NotSupportedException(string message, Exception innerException) : base(message, innerException) { }

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="NotSupportedException"/> с сообщением по умолчанию.
        /// </summary>
        public NotSupportedException() : base(ResourceStrings.Exception_CannotChangeBlockCount) { }

    }

}
