﻿using System;
using System.Globalization;

namespace TrainzDevTeam.Configuration
{

    /// <summary>
    /// Исключение, выбрасываемое, когда раздел конфигурации уже существует.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1032")]
    public class KeyAlreadyExistException : Exception 
    {

        #region Конструктор

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="KeyAlreadyExistException"/>.
        /// </summary>
        /// <param name="keyPath">Путь к разделу конфигурации, связанному с исключением.</param>
        public KeyAlreadyExistException(string keyPath) : base(string.Format(CultureInfo.CurrentCulture, ResourceStrings.Exception_KeyAlreadyExist, keyPath)) => KeyPath = keyPath;

        #endregion

        #region Свойства

        /// <summary>
        /// Возвращает путь к разделу конфигурации, с которым связано событие.
        /// </summary>
        public string KeyPath { get; }

        #endregion

    }
}
