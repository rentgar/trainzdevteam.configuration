﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Linq;
using System.Globalization;

namespace TrainzDevTeam.Configuration
{
    internal sealed class ConfigFile : Config
    {

        #region Данные класса

        internal static byte[] signature = { 79, 176, 67, 188, 70, 185, 84, 171, 83, 172 };
        private FileStream _fstream;
        private object _syncroot;

        #endregion

        #region Конструктор

        internal ConfigFile(FileStream stream)
        {
            _fstream = stream;
            _syncroot = new object();
        }

        #endregion

        #region Методы

        #region Статические

        internal static int BlockPosition(ushort blocksCount, ushort index) => 14 + blocksCount * 2 + 64 * (index - 1);

        internal static FileStream CreateConfigFile(string path, ushort blocksCount, bool canChangeBlockCount)
        {
            if (blocksCount < 32 || blocksCount > 65534) throw new ArgumentOutOfRangeException(nameof(blocksCount), ResourceStrings.Exception_BlockCount);
            var fstream = new FileStream(path, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite);
            fstream.SetLength(blocksCount * 2 + signature.Length + 4);
            fstream.Write(signature, 0, signature.Length);
            fstream.WriteByte(1);
            fstream.WriteByte((byte)(canChangeBlockCount ? 0xFF : 0x00));
            fstream.Write(BitConverter.GetBytes(blocksCount), 0, 2);
            return fstream;
        }

        internal static ConfigFile FromFile(string path)
        {
            var stream = new FileStream(path, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);
            try {
                var filesignature = new byte[signature.Length];
                if (stream.Read(filesignature, 0, filesignature.Length) != filesignature.Length) throw new WrongFileFormatException(path);
                if (!signature.SequenceEqual(filesignature)) throw new WrongFileFormatException(path);
                return new ConfigFile(stream);
            } catch {
                stream.Dispose();
                throw;
            }
        }

        #endregion

        #region Служебные

        private ushort ReadTableItem(ushort index)
        {
            var buffer = new byte[2];
            _fstream.Seek(14 + (index - 1) * 2, SeekOrigin.Begin);
            _fstream.Read(buffer, 0, buffer.Length);
            return BitConverter.ToUInt16(buffer, 0);
        }

        private void WriteTableItem(ushort index, ushort value)
        {
            _fstream.Seek(14 + (index - 1) * 2, SeekOrigin.Begin);
            _fstream.Write(BitConverter.GetBytes(value), 0, 2);
        }

        private ushort GetFirstFreeBlockIndex()
        {
            using (var reader = new BinaryReader(_fstream, Encoding.UTF8, true)) {
                _fstream.Seek(12, SeekOrigin.Begin);
                var blockscount = reader.ReadUInt16();
                for (int i = 1; i <= blockscount; ++i)
                    if (reader.ReadUInt16() == 0x0000) return (ushort)i;
            }
            return 0x0000;
        }

        private ushort GetLastBlock(ushort firstBlockIndex)
        {
            var index = firstBlockIndex;
            while (true) {
                var nextindex = ReadTableItem(index);
                if (nextindex == 0xffff) return index;
                else index = nextindex;
            }
        }

        private ushort AddInfo(ushort firstBlockIndex, string name, ushort size, DataType type)
        {
            var newindex = GetFirstFreeBlockIndex();
            if (newindex == 0x0000) throw new NotEnoughFreeSpaceException();
            WriteTableItem(newindex, 0xffff);
            if (firstBlockIndex != 0x0000) WriteTableItem(GetLastBlock(firstBlockIndex), newindex);
            var blockname = Encoding.GetEncoding(1251).GetBytes(name);
            var blockcount = BlocksCount;
            if (blockname.Length < 58) Array.Resize(ref blockname, 58);
            _fstream.Seek(14 + blockcount * 2 + 64 * (newindex - 1), SeekOrigin.Begin);
            _fstream.WriteByte((byte)name.Length);
            _fstream.Write(blockname, 0, blockname.Length);
            _fstream.WriteByte((byte)type);
            _fstream.Write(BitConverter.GetBytes(size), 0, 2);
            _fstream.Write(new byte[] { 0, 0 }, 0, 2);
            return newindex;
        }

        private void DeleteKey(ushort parentKeyIndex, IndexInfo keyIndexInfo)
        {
            var blockscount = (ushort)BlocksCount;
            var keydataindex = GetConfigItemFirstDataBlockIndex(blockscount, keyIndexInfo.Index);
            if (keydataindex > 1 && keydataindex != 0xffff) {
                do {
                    if (GetConfigItemType(blockscount, keydataindex) == DataType.Key) DeleteKey(keyIndexInfo.Index, new IndexInfo(0, keydataindex));
                    else DeleteValue(keyIndexInfo.Index, new IndexInfo(0, keydataindex));
                    keydataindex = GetConfigItemFirstDataBlockIndex(blockscount, keyIndexInfo.Index);
                } while (keydataindex != 0x0000 && (parentKeyIndex != 0x0000 || ReadTableItem(1) != 0x0000));
            }
            var nextindex = ReadTableItem(keyIndexInfo.Index);
            if (nextindex == 0xffff && keyIndexInfo.PreviousIndex != 0x0000) WriteTableItem(keyIndexInfo.PreviousIndex, 0xffff);
            else if (keyIndexInfo.PreviousIndex != 0x0000) WriteTableItem(keyIndexInfo.PreviousIndex, nextindex);
            else if (parentKeyIndex != 0x0000 && nextindex == 0xffff) SetConfigItemFirstDataBlockIndex(blockscount, parentKeyIndex, 0x0000);
            else if (parentKeyIndex != 0x0000) SetConfigItemFirstDataBlockIndex(blockscount, parentKeyIndex, nextindex);
            if (parentKeyIndex == 0x0000 && keyIndexInfo.PreviousIndex == 0x0000 && nextindex != 0xffff && keyIndexInfo.Index == 1) {
                var mustdeleteindex = nextindex;
                var buffer = new byte[64];
                nextindex = ReadTableItem(nextindex);
                WriteTableItem(keyIndexInfo.Index, nextindex);
                _fstream.Seek(14 + blockscount * 2 + 64 * (mustdeleteindex - 1), SeekOrigin.Begin);
                _fstream.Read(buffer, 0, buffer.Length);
                _fstream.Seek(14 + blockscount * 2, SeekOrigin.Begin);
                _fstream.Write(buffer, 0, buffer.Length);
                WriteTableItem(mustdeleteindex, 0x0000);
            } else WriteTableItem(keyIndexInfo.Index, 0x0000);
        }

        private void DeleteValue(ushort keyIndex, IndexInfo valueIndexInfo)
        {
            var blockscount = (ushort)BlocksCount;
            var dataindex = GetConfigItemFirstDataBlockIndex(blockscount, valueIndexInfo.Index);
            var nextindex = ReadTableItem(valueIndexInfo.Index);
            if (nextindex == 0xffff && valueIndexInfo.PreviousIndex != 0x0000) WriteTableItem(valueIndexInfo.PreviousIndex, 0xffff);
            else if (valueIndexInfo.PreviousIndex != 0x0000) WriteTableItem(valueIndexInfo.PreviousIndex, nextindex);
            else if (keyIndex != 0x0000 && nextindex == 0xffff) SetConfigItemFirstDataBlockIndex(blockscount, keyIndex, 0x0000);
            else if (keyIndex != 0x0000) SetConfigItemFirstDataBlockIndex(blockscount, keyIndex, nextindex);
            if (keyIndex == 0x0000 && valueIndexInfo.PreviousIndex == 0x0000 && nextindex != 0xffff && valueIndexInfo.Index == 0x0001) {
                var mustdeleteindex = nextindex;
                var buffer = new byte[64];
                nextindex = ReadTableItem(nextindex);
                WriteTableItem(valueIndexInfo.Index, nextindex);
                _fstream.Seek(14 + blockscount * 2 + 64 * (mustdeleteindex - 1), SeekOrigin.Begin);
                _fstream.Read(buffer, 0, buffer.Length);
                _fstream.Seek(14 + blockscount * 2, SeekOrigin.Begin);
                _fstream.Write(buffer, 0, buffer.Length);
                WriteTableItem(mustdeleteindex, 0x0000);
            } else WriteTableItem(valueIndexInfo.Index, 0x0000);
            if (dataindex > 0x0001 && dataindex != 0xffff) {
                do {
                    var lastdataindex = dataindex;
                    dataindex = ReadTableItem(dataindex);
                    WriteTableItem(lastdataindex, 0x0000);
                } while (dataindex != 0xffff);
            }
        }

        private void SetValue(string[] path, string name, byte[] data, DataType dataType)
        {
            if (data.Length > 0xffff) throw new TooLongDataException();
            lock (_syncroot) {
                var blockscount = (ushort)BlocksCount;
                var datablockcount = (ushort)Math.Ceiling(data.Length / 64d);
                ushort keyindex = 0, keydataindex = 0, valueindex = 0;
                if (path.Length == 1 && path[0].Length == 0) {
                    if (ReadTableItem(0x0001) != 0x0000) {
                        valueindex = SearchConfigItemInfoIndex(0x0001, name, false).Index;
                        keydataindex = 0x0001;
                    }
                } else {
                    if (ReadTableItem(0x0001) == 0x0000) throw new KeyNotFoundException($"/{string.Join("/", path)}");
                    var keyinfo = GetPath(path);
                    if (keyinfo == KeyIndexInfo.Empty) throw new KeyNotFoundException($"/{string.Join("/", path)}");
                    if (keyinfo.KeyDataIndex != 0x0000) valueindex = SearchConfigItemInfoIndex(keyinfo.KeyDataIndex, name, false).Index;
                    keyindex = keyinfo.KeyIndex;
                    keydataindex = keyinfo.KeyDataIndex;
                }
                if (valueindex != 0x0000) {
                    if (datablockcount >= GetValueDataBlockCount(valueindex) + FreeBlocksCount) throw new NotEnoughFreeSpaceException(ResourceStrings.Exception_NotEnoughFreeSpace_SetValue);
                    var valuedataindex = GetConfigItemFirstDataBlockIndex(blockscount, valueindex);
                    ushort blockindex = 1, newblockindex = valuedataindex, lastindex = 0x0000;
                    var isnewblock = valuedataindex == 0x0000 && data.Length > 0;
                    do {
                        if (isnewblock) {
                            newblockindex = GetFirstFreeBlockIndex();
                            if (newblockindex == 0x0000) throw new NotEnoughFreeSpaceException(ResourceStrings.Exception_NotEnoughFreeSpace_SetValue);
                            WriteTableItem(newblockindex, 0xffff);
                            if (blockindex == 0x0001) SetConfigItemFirstDataBlockIndex(blockscount, valueindex, newblockindex);
                            else WriteTableItem(lastindex, newblockindex);
                            lastindex = newblockindex;
                            if (blockindex == datablockcount) newblockindex = 0xffff;
                        } else if (valuedataindex != 0x0000) {
                            lastindex = newblockindex;
                            newblockindex = ReadTableItem(newblockindex);
                            if (blockindex == datablockcount) WriteTableItem(lastindex, 0xffff);
                            else if (blockindex > datablockcount || data.Length == 0) WriteTableItem(lastindex, 0x0000);
                            isnewblock = newblockindex == 0x0000;
                        }
                        if (blockindex <= datablockcount) {
                            _fstream.Seek(14 + blockscount * 2 + 64 * (lastindex - 1), SeekOrigin.Begin);
                            _fstream.Write(data, 64 * (blockindex - 1), Math.Min(data.Length - 64 * (blockindex - 1), 64));
                        }
                        blockindex++;
                    } while (blockindex <= datablockcount || (newblockindex != 0xffff && !(data.Length == 0 && valuedataindex == 0x0000)));
                    SetConfigItemDataSize(blockscount, valueindex, (ushort)data.Length);
                    SetConfigItemType(blockscount, valueindex, dataType);
                    if (data.Length == 0) SetConfigItemFirstDataBlockIndex(blockscount, valueindex, 0x0000);
                } else {
                    if (datablockcount + 1 > FreeBlocksCount) throw new NotEnoughFreeSpaceException(ResourceStrings.Exception_NotEnoughFreeSpace_SetValue);
                    var newindex = AddInfo(keydataindex, name, (ushort)data.Length, dataType);
                    if (keydataindex == 0x0000 && keyindex != 0x0000) SetConfigItemFirstDataBlockIndex(blockscount, keyindex, newindex);
                    ushort lastindex = 0x0000;
                    for (int i = 1; i <= datablockcount; ++i) {
                        var newbloxkindex = GetFirstFreeBlockIndex();
                        if (newbloxkindex == 0x0000) throw new NotEnoughFreeSpaceException(ResourceStrings.Exception_NotEnoughFreeSpace_SetValue);
                        if (i == 1) SetConfigItemFirstDataBlockIndex(blockscount, newindex, newbloxkindex);
                        else WriteTableItem(lastindex, newbloxkindex);
                        lastindex = newbloxkindex;
                        WriteTableItem(newbloxkindex, 0xffff);
                        _fstream.Seek(14 + blockscount * 2 + 64 * (lastindex - 1), SeekOrigin.Begin);
                        _fstream.Write(data, 64 * (i - 1), Math.Min(data.Length - 64 * (i - 1), 64));
                    }
                }
                _fstream.Flush();
            }
        }

        private string[] GetNameKeys(ushort keyIndex)
        {
            var retnames = new List<string>();
            var blockscount = (ushort)BlocksCount;
            do {
                if (GetConfigItemType(blockscount, keyIndex) == DataType.Key) {
                    var name = GetConfigItemName(blockscount, keyIndex);
                    if (!string.IsNullOrWhiteSpace(name)) retnames.Add(name);
                }
                keyIndex = ReadTableItem(keyIndex);
            } while (keyIndex != 0xffff);
            return retnames.ToArray();
        }

        private string[] GetNameValues(ushort keyIndex)
        {
            var retnames = new List<string>();
            var blockscount = (ushort)BlocksCount;
            do {
                if (GetConfigItemType(blockscount, keyIndex) != DataType.Key) {
                    var name = GetConfigItemName(blockscount, keyIndex);
                    if (!string.IsNullOrWhiteSpace(name)) retnames.Add(name);
                }
                keyIndex = ReadTableItem(keyIndex);
            } while (keyIndex != 0xffff);
            return retnames.ToArray();
        }

        private ValueInfo[] GetInfoValues(ushort keyIndex, string path)
        {
            var retitems = new List<ValueInfo>();
            var blockscount = (ushort)BlocksCount;
            do {
                var itemtype = GetConfigItemType(blockscount, keyIndex);
                if (itemtype != DataType.Key) {
                    var name = GetConfigItemName(blockscount, keyIndex);
                    if (!string.IsNullOrWhiteSpace(name)) retitems.Add(new ValueInfo(path, name, GetValueData(GetConfigItemFirstDataBlockIndex(blockscount, keyIndex), GetConfigItemDataSize(blockscount, keyIndex), itemtype)));
                }
                keyIndex = ReadTableItem(keyIndex);
            } while (keyIndex != 0xffff);
            return retitems.ToArray();
        }







        private void SetConfigItemType(ushort blocksCount, ushort infoBlockIndex, DataType value)
        {
            _fstream.Seek(BlockPosition(blocksCount, infoBlockIndex) + 59, SeekOrigin.Begin);
            _fstream.WriteByte((byte)value);
        }




        private DataType GetConfigItemType(ushort blocksCount, ushort infoBlockIndex)
        {
            _fstream.Seek(BlockPosition(blocksCount, infoBlockIndex) + 59, SeekOrigin.Begin);
            return (DataType)_fstream.ReadByte();
        }

        private string GetConfigItemName(ushort blocksCount, ushort infoBlockIndex)
        {
            _fstream.Seek(BlockPosition(blocksCount, infoBlockIndex), SeekOrigin.Begin);
            var buffer = new byte[_fstream.ReadByte()];
            _fstream.Read(buffer, 0, buffer.Length);
            return (buffer.Length > 0 && buffer.Length <= 58) ? Encoding.GetEncoding(1251).GetString(buffer) : string.Empty;
        }

        private ushort GetConfigItemDataSize(ushort blocksCount, ushort infoBlockIndex)
        {
            _fstream.Seek(BlockPosition(blocksCount, infoBlockIndex) + 60, SeekOrigin.Begin);
            var buffer = new byte[2];
            _fstream.Read(buffer, 0, buffer.Length);
            return BitConverter.ToUInt16(buffer, 0);
        }

        private ushort GetConfigItemFirstDataBlockIndex(ushort blocksCount, ushort infoBlockIndex)
        {
            _fstream.Seek(BlockPosition(blocksCount, infoBlockIndex) + 62, SeekOrigin.Begin);
            var buffer = new byte[2];
            _fstream.Read(buffer, 0, buffer.Length);
            return BitConverter.ToUInt16(buffer, 0);
        }






        private void SetConfigItemName(ushort blocksCount, ushort infoBlockIndex, string value)
        {
            var buffer = Encoding.GetEncoding(1251).GetBytes(value);
            if (buffer.Length < 58) Array.Resize(ref buffer, 58);
            _fstream.Seek(BlockPosition(blocksCount, infoBlockIndex), SeekOrigin.Begin);
            _fstream.WriteByte((byte)value.Length);
            _fstream.Write(buffer, 0, buffer.Length);
        }

        private void SetConfigItemFirstDataBlockIndex(ushort blocksCount, ushort infoBlockIndex, ushort value)
        {
            _fstream.Seek(BlockPosition(blocksCount, infoBlockIndex) + 62, SeekOrigin.Begin);
            _fstream.Write(BitConverter.GetBytes(value), 0, 2);
        }






        private void SetConfigItemDataSize(ushort blocksCount, ushort infoBlockIndex, ushort value)
        {
            _fstream.Seek(BlockPosition(blocksCount, infoBlockIndex) + 60, SeekOrigin.Begin);
            _fstream.Write(BitConverter.GetBytes(value), 0, 2);
        }

        private IndexInfo SearchConfigItemInfoIndex(ushort index, string name, bool iskey)
        {
            var blockscount = (ushort)BlocksCount;
            ushort previndex = 0x0000;
            do {
                var itemtype = GetConfigItemType(blockscount, index);
                if ((iskey && itemtype == DataType.Key) || (!iskey && itemtype != DataType.Key)) {
                    var itemname = GetConfigItemName(blockscount, index);
                    if (!string.IsNullOrWhiteSpace(itemname) && string.Equals(name, itemname, StringComparison.CurrentCultureIgnoreCase)) return new IndexInfo(previndex, index);
                }
                previndex = index;
                index = ReadTableItem(index);
            } while (index != 0xffff);
            return IndexInfo.Empty;
        }

        private KeyIndexInfo GetPath(string[] path)
        {
            ushort keyindex = 0x0000, keydataindex = 0x0001, prevkeyindex = 0x0000, prevkeydataindex = 0x0001;
            var blockscount = (ushort)BlocksCount;
            for (int i = 0; i < path.Length; ++i) {
                keyindex = SearchConfigItemInfoIndex(keydataindex, path[i], true).Index;
                if (keyindex == 0x0000) return KeyIndexInfo.Empty;
                keydataindex = GetConfigItemFirstDataBlockIndex(blockscount, keyindex);
                if (keydataindex == 0x0000 && i != path.Length - 1) return KeyIndexInfo.Empty;
                if (i == path.Length - 2) {
                    prevkeyindex = keyindex;
                    prevkeydataindex = keydataindex;
                }
            }
            return new KeyIndexInfo(prevkeyindex, prevkeydataindex, keyindex, keydataindex);
        }

        private ushort GetValueDataBlockCount(ushort valueIndex)
        {
            var dataindex = GetConfigItemFirstDataBlockIndex((ushort)BlocksCount, valueIndex);
            if (dataindex == 0x0000) return 0;
            ushort retcount = 0;
            do {
                retcount++;
                dataindex = ReadTableItem(dataindex);
            } while (dataindex != 0xffff);
            return retcount;
        }

        private object GetValueData(ushort startIndex, ushort dataSize, DataType dataType)
        {
            var blockscount = (ushort)BlocksCount;
            var buffer = new byte[dataSize];
            int index = 0;
            if (startIndex != 0x0000) {
                while (startIndex != 0xffff) {
                    _fstream.Seek(14 + blockscount * 2 + 64 * (startIndex - 1), SeekOrigin.Begin);
                    index += _fstream.Read(buffer, index, Math.Min(dataSize - index, 64));
                    startIndex = ReadTableItem(startIndex);
                }
            }
            if (index != dataSize) return null;
            switch (dataType) {
                case DataType.String:
                    return Encoding.UTF8.GetString(buffer);
                case DataType.Bytes:
                    return buffer;
                case DataType.SByte:
                    if (dataSize == 1) return Convert.ToSByte(buffer[0]);
                    break;
                case DataType.Byte:
                    if (dataSize == 1) return buffer[0];
                    break;
                case DataType.Int16:
                    if (dataSize == 2) return BitConverter.ToInt16(buffer, 0);
                    break;
                case DataType.Int32:
                    if (dataSize == 4) return BitConverter.ToInt32(buffer, 0);
                    break;
                case DataType.Int64:
                    if (dataSize == 8) return BitConverter.ToInt64(buffer, 0);
                    break;
                case DataType.UInt16:
                    if (dataSize == 2) return BitConverter.ToUInt16(buffer, 0);
                    break;
                case DataType.UInt32:
                    if (dataSize == 4) return BitConverter.ToUInt32(buffer, 0);
                    break;
                case DataType.UInt64:
                    if (dataSize == 8) return BitConverter.ToUInt64(buffer, 0);
                    break;
                case DataType.Bool:
                    if (dataSize == 1) return Convert.ToBoolean(buffer[0]);
                    break;
                case DataType.Single:
                    if (dataSize == 4) return BitConverter.ToSingle(buffer, 0);
                    break;
                case DataType.Double:
                    if (dataSize == 8) return BitConverter.ToDouble(buffer, 0);
                    break;
                case DataType.Decimal:
                    if (dataSize == 16) {
                        var bits = new int[4];
                        for (int i = 0; i < bits.Length; ++i)
                            bits[i] = BitConverter.ToInt32(buffer, i * 4);
                        return new decimal(bits);
                    }
                    break;
                case DataType.GUID:
                    if (dataSize == 16) return new Guid(buffer);
                    break;
                case DataType.Point:
                    if (dataSize == 8) return new Point(BitConverter.ToInt32(buffer, 0), BitConverter.ToInt32(buffer, 4));
                    break;
                case DataType.Size:
                    if (dataSize == 8) return new Size(BitConverter.ToInt32(buffer, 0), BitConverter.ToInt32(buffer, 4));
                    break;
                case DataType.Version:
                    if (dataSize == 16) {
                        var major = BitConverter.ToInt32(buffer, 0);
                        var minor = BitConverter.ToInt32(buffer, 4);
                        var build = BitConverter.ToInt32(buffer, 8);
                        var revision = BitConverter.ToInt32(buffer, 12);
                        return (build >= 0 && revision >= 0) ? new Version(major, minor, build, revision) : (build >= 0) ? new Version(major, minor, build) : new Version(major, minor);
                    }
                    break;
            }
            return null;
        }

        #endregion

        #region Обслуживание

        public override void ChangeBlockCount(ushort newBlockCount)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Config));
            if (newBlockCount < 32 || newBlockCount > 65534) throw new ArgumentOutOfRangeException(nameof(newBlockCount), ResourceStrings.Exception_BlockCount);
            if (!CanChangeBlocksCount) throw new NotSupportedException();
            lock (_syncroot) {
                var blockscount = (ushort)BlocksCount;
                if (newBlockCount < OccupiedBlocksCount) throw new InvalidOperationException(ResourceStrings.Exception_TooSmallBlocksCount);
                if (newBlockCount > blockscount) {
                    var resize = newBlockCount - blockscount;
                    var movedatalength = _fstream.Length - BlockPosition(blockscount, 0x0001);
                    _fstream.SetLength(_fstream.Length + resize * 2);
                    if (movedatalength > 0) {
                        var movedatabuffer = new byte[movedatalength];
                        _fstream.Seek(BlockPosition(blockscount, 0x0001), SeekOrigin.Begin);
                        _fstream.Read(movedatabuffer, 0, movedatabuffer.Length);
                        _fstream.Seek(BlockPosition(blockscount, 0x0001) + resize * 2, SeekOrigin.Begin);
                        _fstream.Write(movedatabuffer, 0, movedatabuffer.Length);
                    }
                    var emptybuffer = new byte[resize * 2];
                    _fstream.Seek(BlockPosition(blockscount, 0x0001), SeekOrigin.Begin);
                    _fstream.Write(emptybuffer, 0, emptybuffer.Length);
                    _fstream.Seek(12, SeekOrigin.Begin);
                    _fstream.Write(BitConverter.GetBytes(newBlockCount), 0, 2);
                    _fstream.Flush();
                } else if (newBlockCount < blockscount) {
                    var filename = _fstream.Name;
                    _fstream.Dispose();
                    try { using (var defrag = new Utilites.Defragmentator(filename)) defrag.Defragmentation(); }
                    finally { _fstream = new FileStream(filename, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite); }
                    var resize = blockscount - newBlockCount;
                    var movedatalength = _fstream.Length - BlockPosition(blockscount, 0x0001);
                    if (movedatalength > 0) {
                        var movedatabuffer = new byte[movedatalength];
                        _fstream.Seek(BlockPosition(blockscount, 0x0001), SeekOrigin.Begin);
                        _fstream.Read(movedatabuffer, 0, movedatabuffer.Length);
                        _fstream.Seek(BlockPosition(blockscount, 0x0001) - resize * 2, SeekOrigin.Begin);
                        _fstream.Write(movedatabuffer, 0, movedatabuffer.Length);
                    }
                    _fstream.Seek(12, SeekOrigin.Begin);
                    _fstream.Write(BitConverter.GetBytes(newBlockCount), 0, 2);
                    _fstream.SetLength(_fstream.Length - resize * 2);
                    _fstream.Flush();
                }
            }
        }

        #endregion

        #region Разделы

        public override void CreateKey(string path, string keyName)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Config));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (keyName == null) throw new ArgumentNullException(nameof(keyName));
            if (!AbsoluteKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            if (!KeyOrValueNameIsValid(keyName)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyName, nameof(keyName));
            var keynames = path.Trim('/').Split('/');
            if (keynames.Length >= MaximumNestingDepth) throw new NestingDepthExceededException(CombinePath(path, keyName));
            lock (_syncroot) {
                if (FreeBlocksCount == 0) throw new NotEnoughFreeSpaceException(ResourceStrings.Exception_NotEnoughFreeSpace_CreateKey);
                if (keynames.Length == 1 && keynames[0].Length == 0) {
                    ushort index = 0x0000;
                    if (ReadTableItem(0x0001) != 0x0000) {
                        if (SearchConfigItemInfoIndex(0x0001, keyName, true).Index != 0x0000) throw new KeyAlreadyExistException(CombinePath(path, keyName));
                        index = 0x0001;
                    }
                    AddInfo(index, keyName, 0, DataType.Key);
                } else {
                    if (ReadTableItem(0x0001) == 0x0000) throw new KeyNotFoundException(CombinePath(path, keyName));
                    var parentinfo = GetPath(keynames);
                    if (parentinfo == KeyIndexInfo.Empty) throw new KeyNotFoundException(CombinePath(path, keyName));
                    if (parentinfo.KeyDataIndex != 0x0000 && SearchConfigItemInfoIndex(parentinfo.KeyDataIndex, keyName, true).Index != 0x0000)
                        throw new KeyAlreadyExistException(CombinePath(path, keyName));
                    var newindex = AddInfo(parentinfo.KeyDataIndex, keyName, 0, DataType.Key);
                    if (parentinfo.KeyDataIndex == 0x0000) SetConfigItemFirstDataBlockIndex((ushort)BlocksCount, parentinfo.KeyIndex, newindex);
                }
                _fstream.Flush();
            }
        }

        public override void RenameKey(string path, string newKeyName)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Config));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (newKeyName == null) throw new ArgumentNullException(nameof(newKeyName));
            if (!AbsoluteKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            if (!KeyOrValueNameIsValid(newKeyName)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyName, nameof(newKeyName));
            var keynames = path.Trim('/').Split('/');
            if (keynames.Length > MaximumNestingDepth) throw new NestingDepthExceededException(ValidPath(path));
            if (keynames.Length == 1 && keynames[0].Length == 0) throw new InvalidOperationException(ResourceStrings.Exception_CannotRenameRootKey);
            lock (_syncroot) {
                if (ReadTableItem(0x0001) == 0x0000) throw new KeyNotFoundException(ValidPath(path));
                var keyinfo = GetPath(keynames);
                if (keyinfo == KeyIndexInfo.Empty) throw new KeyNotFoundException(ValidPath(path));
                if (SearchConfigItemInfoIndex(keyinfo.ParentKeyDataIndex, newKeyName, true).Index != 0x0000) throw new KeyAlreadyExistException("/" + string.Join("/", keynames, 0, keynames.Length - 1) + "/" + newKeyName);
                SetConfigItemName((ushort)BlocksCount, keyinfo.KeyIndex, newKeyName);
                _fstream.Flush();
            }
        }

        public override void DeleteKey(string path, bool recursive)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Config));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (!AbsoluteKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            var keynames = path.Trim('/').Split('/');
            if (keynames.Length > MaximumNestingDepth) throw new NestingDepthExceededException(ValidPath(path));
            if (keynames.Length == 1 && keynames[0].Length == 0) throw new InvalidOperationException(ResourceStrings.Exception_CannotDeleteRootKey);
            lock (_syncroot) {
                if (ReadTableItem(0x0001) == 0x0000) throw new KeyNotFoundException(ValidPath(path));
                var keyinfo = GetPath(keynames);
                if (keyinfo == KeyIndexInfo.Empty) throw new KeyNotFoundException(ValidPath(path));
                if (!recursive && GetConfigItemFirstDataBlockIndex((ushort)BlocksCount, keyinfo.KeyIndex) != 0x0000) throw new KeyNotEmptyException(ValidPath(path));
                ushort pindex = 0x0000, index = keyinfo.ParentKeyDataIndex;
                do {
                    if (index == keyinfo.KeyIndex) break;
                    else pindex = index;
                    index = ReadTableItem(index);
                } while (index != 0xffff);
                DeleteKey(keyinfo.ParentKeyIndex, new IndexInfo(pindex, keyinfo.KeyIndex));
                _fstream.Flush();
            }
        }

        public override string[] GetNameKeys(string path)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Config));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (!AbsoluteKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            var keynames = path.Trim('/').Split('/');
            if (keynames.Length > MaximumNestingDepth) throw new NestingDepthExceededException(ValidPath(path));
            lock (_syncroot) {
                if (keynames.Length == 1 && keynames[0].Length == 0) {
                    if (ReadTableItem(1) != 0x0000) return GetNameKeys(0x0001);
                } else {
                    if (ReadTableItem(0x0001) == 0x0000) throw new KeyNotFoundException(ValidPath(path));
                    var parentinfo = GetPath(keynames);
                    if (parentinfo == KeyIndexInfo.Empty) throw new KeyNotFoundException(ValidPath(path));
                    if (parentinfo.KeyDataIndex != 0x0000) return GetNameKeys(parentinfo.KeyDataIndex);
                }
            }
            return Array.Empty<string>();
        }

        public override bool ExistKey(string path)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Config));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (!AbsoluteKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            var keynames = path.Trim('/').Split('/');
            if (keynames.Length > MaximumNestingDepth) throw new NestingDepthExceededException(ValidPath(path));
            lock (_syncroot) {
                if (keynames.Length != 1 || keynames[0].Length != 0) {
                    if (ReadTableItem(0x0001) == 0x0000) return false;
                    return GetPath(keynames) != KeyIndexInfo.Empty;
                } else return true;
            }
        }

        #endregion

        #region Параметры

        public override void SetValue(string path, string name, byte[] value)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Config));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (name == null) throw new ArgumentNullException(nameof(name));
            if (value == null) throw new ArgumentNullException(nameof(value));
            if (!AbsoluteKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            if (!KeyOrValueNameIsValid(name)) throw new ArgumentException(ResourceStrings.Exception_InvalidValueName, nameof(name));
            var keynames = path.Trim('/').Split('/');
            if (keynames.Length > MaximumNestingDepth) throw new NestingDepthExceededException(ValidPath(path));
            SetValue(keynames, name, value, DataType.Bytes);
        }

        public override void SetValue(string path, string name, byte value)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Config));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (name == null) throw new ArgumentNullException(nameof(name));
            if (!AbsoluteKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            if (!KeyOrValueNameIsValid(name)) throw new ArgumentException(ResourceStrings.Exception_InvalidValueName, nameof(name));
            var keynames = path.Trim('/').Split('/');
            if (keynames.Length > MaximumNestingDepth) throw new NestingDepthExceededException(ValidPath(path));
            SetValue(keynames, name, new byte[] { value }, DataType.Byte);
        }

        public override void SetValue(string path, string name, short value)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Config));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (name == null) throw new ArgumentNullException(nameof(name));
            if (!AbsoluteKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            if (!KeyOrValueNameIsValid(name)) throw new ArgumentException(ResourceStrings.Exception_InvalidValueName, nameof(name));
            var keynames = path.Trim('/').Split('/');
            if (keynames.Length > MaximumNestingDepth) throw new NestingDepthExceededException(ValidPath(path));
            SetValue(keynames, name, BitConverter.GetBytes(value), DataType.Int16);
        }

        public override void SetValue(string path, string name, int value)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Config));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (name == null) throw new ArgumentNullException(nameof(name));
            if (!AbsoluteKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            if (!KeyOrValueNameIsValid(name)) throw new ArgumentException(ResourceStrings.Exception_InvalidValueName, nameof(name));
            var keynames = path.Trim('/').Split('/');
            if (keynames.Length > MaximumNestingDepth) throw new NestingDepthExceededException(ValidPath(path));
            SetValue(keynames, name, BitConverter.GetBytes(value), DataType.Int32);
        }

        public override void SetValue(string path, string name, long value)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Config));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (name == null) throw new ArgumentNullException(nameof(name));
            if (!AbsoluteKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            if (!KeyOrValueNameIsValid(name)) throw new ArgumentException(ResourceStrings.Exception_InvalidValueName, nameof(name));
            var keynames = path.Trim('/').Split('/');
            if (keynames.Length > MaximumNestingDepth) throw new NestingDepthExceededException(ValidPath(path));
            SetValue(keynames, name, BitConverter.GetBytes(value), DataType.Int64);
        }

        public override void SetValue(string path, string name, ushort value)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Config));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (name == null) throw new ArgumentNullException(nameof(name));
            if (!AbsoluteKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            if (!KeyOrValueNameIsValid(name)) throw new ArgumentException(ResourceStrings.Exception_InvalidValueName, nameof(name));
            var keynames = path.Trim('/').Split('/');
            if (keynames.Length > MaximumNestingDepth) throw new NestingDepthExceededException(ValidPath(path));
            SetValue(keynames, name, BitConverter.GetBytes(value), DataType.UInt16);
        }

        public override void SetValue(string path, string name, uint value)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Config));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (name == null) throw new ArgumentNullException(nameof(name));
            if (!AbsoluteKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            if (!KeyOrValueNameIsValid(name)) throw new ArgumentException(ResourceStrings.Exception_InvalidValueName, nameof(name));
            var keynames = path.Trim('/').Split('/');
            if (keynames.Length > MaximumNestingDepth) throw new NestingDepthExceededException(ValidPath(path));
            SetValue(keynames, name, BitConverter.GetBytes(value), DataType.UInt32);
        }

        public override void SetValue(string path, string name, ulong value)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Config));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (name == null) throw new ArgumentNullException(nameof(name));
            if (!AbsoluteKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            if (!KeyOrValueNameIsValid(name)) throw new ArgumentException(ResourceStrings.Exception_InvalidValueName, nameof(name));
            var keynames = path.Trim('/').Split('/');
            if (keynames.Length > MaximumNestingDepth) throw new NestingDepthExceededException(ValidPath(path));
            SetValue(keynames, name, BitConverter.GetBytes(value), DataType.UInt64);
        }

        public override void SetValue(string path, string name, bool value)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Config));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (name == null) throw new ArgumentNullException(nameof(name));
            if (!AbsoluteKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            if (!KeyOrValueNameIsValid(name)) throw new ArgumentException(ResourceStrings.Exception_InvalidValueName, nameof(name));
            var keynames = path.Trim('/').Split('/');
            if (keynames.Length > MaximumNestingDepth) throw new NestingDepthExceededException(ValidPath(path));
            SetValue(keynames, name, BitConverter.GetBytes(value), DataType.Bool);
        }

        public override void SetValue(string path, string name, decimal value)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Config));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (name == null) throw new ArgumentNullException(nameof(name));
            if (!AbsoluteKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            if (!KeyOrValueNameIsValid(name)) throw new ArgumentException(ResourceStrings.Exception_InvalidValueName, nameof(name));
            var keynames = path.Trim('/').Split('/');
            if (keynames.Length > MaximumNestingDepth) throw new NestingDepthExceededException(ValidPath(path));
            var bits = decimal.GetBits(value);
            var data = new byte[16];
            for (int i = 0; i < bits.Length; ++i)
                Array.Copy(BitConverter.GetBytes(bits[i]), 0, data, i * 4, 4);
            SetValue(keynames, name, data, DataType.Decimal);
        }

        public override void SetValue(string path, string name, double value)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Config));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (name == null) throw new ArgumentNullException(nameof(name));
            if (!AbsoluteKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            if (!KeyOrValueNameIsValid(name)) throw new ArgumentException(ResourceStrings.Exception_InvalidValueName, nameof(name));
            var keynames = path.Trim('/').Split('/');
            if (keynames.Length > MaximumNestingDepth) throw new NestingDepthExceededException(ValidPath(path));
            SetValue(keynames, name, BitConverter.GetBytes(value), DataType.Double);
        }

        public override void SetValue(string path, string name, sbyte value)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Config));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (name == null) throw new ArgumentNullException(nameof(name));
            if (!AbsoluteKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            if (!KeyOrValueNameIsValid(name)) throw new ArgumentException(ResourceStrings.Exception_InvalidValueName, nameof(name));
            var keynames = path.Trim('/').Split('/');
            if (keynames.Length > MaximumNestingDepth) throw new NestingDepthExceededException(ValidPath(path));
            SetValue(keynames, name, new byte[] { unchecked((byte)value) }, DataType.SByte);
        }

        public override void SetValue(string path, string name, float value)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Config));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (name == null) throw new ArgumentNullException(nameof(name));
            if (!AbsoluteKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            if (!KeyOrValueNameIsValid(name)) throw new ArgumentException(ResourceStrings.Exception_InvalidValueName, nameof(name));
            var keynames = path.Trim('/').Split('/');
            if (keynames.Length > MaximumNestingDepth) throw new NestingDepthExceededException(ValidPath(path));
            SetValue(keynames, name, BitConverter.GetBytes(value), DataType.Single);
        }

        public override void SetValue(string path, string name, string value)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Config));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (name == null) throw new ArgumentNullException(nameof(name));
            if (value == null) throw new ArgumentNullException(nameof(value));
            if (!AbsoluteKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            if (!KeyOrValueNameIsValid(name)) throw new ArgumentException(ResourceStrings.Exception_InvalidValueName, nameof(name));
            var keynames = path.Trim('/').Split('/');
            if (keynames.Length > MaximumNestingDepth) throw new NestingDepthExceededException(ValidPath(path));
            SetValue(keynames, name, Encoding.UTF8.GetBytes(value), DataType.String);
        }

        public override void SetValue(string path, string name, Guid value)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Config));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (name == null) throw new ArgumentNullException(nameof(name));
            if (!AbsoluteKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            if (!KeyOrValueNameIsValid(name)) throw new ArgumentException(ResourceStrings.Exception_InvalidValueName, nameof(name));
            var keynames = path.Trim('/').Split('/');
            if (keynames.Length > MaximumNestingDepth) throw new NestingDepthExceededException(ValidPath(path));
            SetValue(keynames, name, value.ToByteArray(), DataType.GUID);
        }

        public override void SetValue(string path, string name, Point value)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Config));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (name == null) throw new ArgumentNullException(nameof(name));
            if (!AbsoluteKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            if (!KeyOrValueNameIsValid(name)) throw new ArgumentException(ResourceStrings.Exception_InvalidValueName, nameof(name));
            var keynames = path.Trim('/').Split('/');
            if (keynames.Length > MaximumNestingDepth) throw new NestingDepthExceededException(ValidPath(path));
            SetValue(keynames, name, BitConverter.GetBytes(value.X).Concat(BitConverter.GetBytes(value.Y)).ToArray(), DataType.Point);
        }

        public override void SetValue(string path, string name, Size value)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Config));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (name == null) throw new ArgumentNullException(nameof(name));
            if (!AbsoluteKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            if (!KeyOrValueNameIsValid(name)) throw new ArgumentException(ResourceStrings.Exception_InvalidValueName, nameof(name));
            var keynames = path.Trim('/').Split('/');
            if (keynames.Length > MaximumNestingDepth) throw new NestingDepthExceededException(ValidPath(path));
            SetValue(keynames, name, BitConverter.GetBytes(value.Width).Concat(BitConverter.GetBytes(value.Height)).ToArray(), DataType.Size);
        }

        public override void SetValue(string path, string name, Version value)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Config));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (name == null) throw new ArgumentNullException(nameof(name));
            if (value == null) throw new ArgumentNullException(nameof(value));
            if (!AbsoluteKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            if (!KeyOrValueNameIsValid(name)) throw new ArgumentException(ResourceStrings.Exception_InvalidValueName, nameof(name));
            var keynames = path.Trim('/').Split('/');
            if (keynames.Length > MaximumNestingDepth) throw new NestingDepthExceededException(ValidPath(path));
            SetValue(keynames, name, BitConverter.GetBytes(value.Major).Concat(BitConverter.GetBytes(value.Minor)).
                Concat(BitConverter.GetBytes(value.Build)).Concat(BitConverter.GetBytes(value.Revision)).ToArray(), DataType.Version);
        }

        public override string[] GetNameValues(string path)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Config));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (!AbsoluteKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            var keynames = path.Trim('/').Split('/');
            if (keynames.Length > MaximumNestingDepth) throw new NestingDepthExceededException(ValidPath(path));
            lock (_syncroot) {
                if (keynames.Length == 1 && keynames[0].Length == 0) {
                    if (ReadTableItem(1) != 0x0000) return GetNameValues(0x0001);
                } else {
                    if (ReadTableItem(0x0001) == 0x0000) throw new KeyNotFoundException(ValidPath(path));
                    var keyinfo = GetPath(keynames);
                    if (keyinfo == KeyIndexInfo.Empty) throw new KeyNotFoundException(ValidPath(path));
                    if (keyinfo.KeyDataIndex != 0x0000) return GetNameValues(keyinfo.KeyDataIndex);
                }
            }
            return Array.Empty<string>();
        }

        public override ValueInfo[] GetInfoValues(string path)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Config));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (!AbsoluteKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            var keynames = path.Trim('/').Split('/');
            if (keynames.Length > MaximumNestingDepth) throw new NestingDepthExceededException(ValidPath(path));
            lock (_syncroot) {
                if (keynames.Length == 1 && keynames[0].Length == 0) {
                    if (ReadTableItem(1) != 0x0000) return GetInfoValues(0x0001, path);
                } else {
                    if (ReadTableItem(0x0001) == 0x0000) throw new KeyNotFoundException(ValidPath(path));
                    var keyinfo = GetPath(keynames);
                    if (keyinfo == KeyIndexInfo.Empty) throw new KeyNotFoundException(ValidPath(path));
                    if (keyinfo.KeyDataIndex != 0x0000) return GetInfoValues(keyinfo.KeyDataIndex, ValidPath(path));
                }
            }
            return Array.Empty<ValueInfo>();
        }

        public override void RenameValue(string path, string name, string newName)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Config));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (name == null) throw new ArgumentNullException(nameof(name));
            if (newName == null) throw new ArgumentNullException(nameof(newName));
            if (!AbsoluteKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            if (!KeyOrValueNameIsValid(name)) throw new ArgumentException(ResourceStrings.Exception_InvalidValueName, nameof(name));
            if (!KeyOrValueNameIsValid(newName)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyName, nameof(newName));
            var keynames = path.Trim('/').Split('/');
            if (keynames.Length > MaximumNestingDepth) throw new NestingDepthExceededException(ValidPath(path));
            lock (_syncroot) {
                if (ReadTableItem(1) == 0x0000) throw new KeyNotFoundException(ValidPath(path));
                ushort keydataindex = 0x0001;
                if (keynames.Length > 1 || keynames[0].Length != 0) {
                    var keyinfo = GetPath(keynames);
                    if (keyinfo == KeyIndexInfo.Empty) throw new KeyNotFoundException(ValidPath(path));
                    if (keyinfo.KeyDataIndex == 0x0000) throw new ValueNotFoundException(ValidPath(path), name);
                    else keydataindex = keyinfo.KeyDataIndex;
                }
                var valueindex = SearchConfigItemInfoIndex(keydataindex, name, false).Index;
                if (valueindex == 0x0000) throw new ValueNotFoundException(ValidPath(path), name);
                if (SearchConfigItemInfoIndex(keydataindex, newName, false).Index != 0x0000) throw new ValueAlreadyExistException(ValidPath(path), newName);
                SetConfigItemName((ushort)BlocksCount, valueindex, newName);
                _fstream.Flush();
            }
        }

        public override ValueInfo GetValueInfo(string path, string name)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Config));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (name == null) throw new ArgumentNullException(nameof(name));
            if (!AbsoluteKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            if (!KeyOrValueNameIsValid(name)) throw new ArgumentException(ResourceStrings.Exception_InvalidValueName, nameof(name));
            var keynames = path.Trim('/').Split('/');
            if (keynames.Length > MaximumNestingDepth) throw new NestingDepthExceededException(ValidPath(path));
            lock (_syncroot) {
                if (ReadTableItem(1) == 0x0000) throw new KeyNotFoundException(ValidPath(path));
                ushort keydataindex = 0x0001;
                if (keynames.Length > 1 || keynames[0].Length != 0) {
                    var keyinfo = GetPath(keynames);
                    if (keyinfo == KeyIndexInfo.Empty) throw new KeyNotFoundException(ValidPath(path));
                    if (keyinfo.KeyDataIndex == 0x0000) throw new ValueNotFoundException(ValidPath(path), name);
                    else keydataindex = keyinfo.KeyDataIndex;
                }
                var valueindex = SearchConfigItemInfoIndex(keydataindex, name, false).Index;
                if (valueindex == 0x0000) throw new ValueNotFoundException(ValidPath(path), name);
                var blockscount = (ushort)BlocksCount;
                return new ValueInfo(ValidPath(path), name, GetValueData(GetConfigItemFirstDataBlockIndex(blockscount, valueindex), GetConfigItemDataSize(blockscount, valueindex), GetConfigItemType(blockscount, valueindex)));
            }
        }

        public override object GetValue(string path, string name, object defaultValue, bool checkType)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Config));
            if (defaultValue == null) throw new ArgumentNullException(nameof(defaultValue));
            try {
                var valueinfo = GetValueInfo(path, name);
                if (valueinfo.ValueIsError) return defaultValue;
                if (checkType && valueinfo.Type != defaultValue.GetType()) return defaultValue;
                return valueinfo.Value;
            }
            catch (KeyNotFoundException) { return defaultValue; }
            catch (ValueNotFoundException) { return defaultValue; }
        }

        public override object GetValue(string path, string name, object defaultValue) => GetValue(path, name, defaultValue);

        public override object GetValue(string path, string name)
        {
            var valueinfo = GetValueInfo(path, name);
            if (valueinfo.ValueIsError) throw new ValueNotFoundException(ValidPath(path), name);
            return valueinfo.Value;
        }

        public override string GetValueAsString(string path, string name, string defaultValue = null, bool canConvertType = true)
        {
            try {
                var valueinfo = GetValueInfo(path, name);
                if (valueinfo.ValueIsError) return defaultValue;
                if (valueinfo.Type == typeof(string)) return (string)valueinfo.Value;
                if (canConvertType) {
                    if (valueinfo.Type == typeof(byte[])) return Encoding.ASCII.GetString((byte[])valueinfo.Value);
                    else return valueinfo.Value.ToString();
                }
            }
            catch (KeyNotFoundException) { }
            catch (ValueNotFoundException) { }
            return defaultValue;
        }

        public override bool GetValueAsBool(string path, string name, bool defaultValue = false, bool canConvertType = true)
        {
            try {
                var valueinfo = GetValueInfo(path, name);
                if (valueinfo.ValueIsError) return defaultValue;
                if (valueinfo.Type == typeof(bool)) return (bool)valueinfo.Value;
                if (canConvertType) {
                    if (valueinfo.Type == typeof(Guid)) return (Guid)valueinfo.Value != Guid.Empty;
                    else if (valueinfo.Type == typeof(string)) return ((string)valueinfo.Value).Length != 0;
                    else if (valueinfo.Type == typeof(Point)) return ((Point)valueinfo.Value).X != 0 || ((Point)valueinfo.Value).Y != 0;
                    else if (valueinfo.Type == typeof(Size)) return ((Size)valueinfo.Value).Width != 0 || ((Size)valueinfo.Value).Height != 0;
                    else if (valueinfo.Type == typeof(Version)) return defaultValue;
                    else return Convert.ToBoolean(valueinfo.Value, CultureInfo.CurrentCulture);
                }
            }
            catch (KeyNotFoundException) { }
            catch (ValueNotFoundException) { }
            return defaultValue;
        }

        public override Guid GetValueAsGuid(string path, string name, Guid defaultValue = default, bool canConvertType = true)
        {
            try {
                var valueinfo = GetValueInfo(path, name);
                if (valueinfo.ValueIsError) return defaultValue;
                if (valueinfo.Type == typeof(Guid)) return (Guid)valueinfo.Value;
                if (canConvertType) {
                    if (valueinfo.Type == typeof(byte[]) && ((byte[])valueinfo.Value).Length == 16) return new Guid((byte[])valueinfo.Value);
                    else if (valueinfo.Type == typeof(string) && Guid.TryParse((string)valueinfo.Value, out var retguid)) return retguid;
                }
            }
            catch (KeyNotFoundException) { }
            catch (ValueNotFoundException) { }
            return defaultValue;
        }

        public override Version GetValueAsVersion(string path, string name, Version defaultValue = null, bool canConvertType = true)
        {
            try {
                var valueinfo = GetValueInfo(path, name);
                if (valueinfo.ValueIsError) return defaultValue;
                if (valueinfo.Type == typeof(Version)) return (Version)valueinfo.Value;
                else if (canConvertType && valueinfo.Type == typeof(string) && Version.TryParse((string)valueinfo.Value, out var retversion)) return retversion;
            }
            catch (KeyNotFoundException) { }
            catch (ValueNotFoundException) { }
            return defaultValue;
        }

        public override TValue GetValueAs<TValue>(string path, string name, TValue defaultValue = default)
        {
            try {
                var valueinfo = GetValueInfo(path, name);
                if (valueinfo.ValueIsError) return defaultValue;
                if (valueinfo.Type == typeof(TValue)) return (TValue)valueinfo.Value;
            }
            catch (KeyNotFoundException) { }
            catch (ValueNotFoundException) { }
            return defaultValue;
        }

        public override void DeleteValue(string path, string name)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Config));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (name == null) throw new ArgumentNullException(nameof(name));
            if (!AbsoluteKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            if (!KeyOrValueNameIsValid(name)) throw new ArgumentException(ResourceStrings.Exception_InvalidValueName, nameof(name));
            var keynames = path.Trim('/').Split('/');
            if (keynames.Length > MaximumNestingDepth) throw new NestingDepthExceededException(ValidPath(path));
            lock (_syncroot) {
                if (ReadTableItem(1) == 0x0000) throw new KeyNotFoundException(ValidPath(path));
                ushort keydataindex = 0x0001, keyindex = 0x0000;
                if (keynames.Length > 1 || keynames[0].Length != 0) {
                    var keyinfo = GetPath(keynames);
                    if (keyinfo == KeyIndexInfo.Empty) throw new KeyNotFoundException(ValidPath(path));
                    if (keyinfo.KeyDataIndex == 0x0000) throw new ValueNotFoundException(ValidPath(path), name);
                    keydataindex = keyinfo.KeyDataIndex;
                    keyindex = keyinfo.KeyIndex;
                }
                var valueindexinfo = SearchConfigItemInfoIndex(keydataindex, name, false);
                if (valueindexinfo == IndexInfo.Empty) throw new ValueNotFoundException(ValidPath(path), name);
                DeleteValue(keyindex, valueindexinfo);
                _fstream.Flush();
            }
        }

        public override bool ExistValue(string path, string name)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Config));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (name == null) throw new ArgumentNullException(nameof(name));
            if (!AbsoluteKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            if (!KeyOrValueNameIsValid(name)) throw new ArgumentException(ResourceStrings.Exception_InvalidValueName, nameof(name));
            var keynames = path.Trim('/').Split('/');
            if (keynames.Length > MaximumNestingDepth) throw new NestingDepthExceededException(ValidPath(path));
            lock (_syncroot) {
                if (ReadTableItem(0x0001) == 0x0000) {
                    if (keynames.Length > 1 || keynames[0].Length != 0) throw new KeyNotFoundException(ValidPath(path));
                    else return false;
                }
                ushort keydataindex = 0x0001;
                if (keynames.Length > 1 || keynames[0].Length != 0) {
                    var keyinfo = GetPath(keynames);
                    if (keyinfo == KeyIndexInfo.Empty) throw new KeyNotFoundException(ValidPath(path));
                    if (keyinfo.KeyDataIndex == 0x0000) return false;
                    else keydataindex = keyinfo.KeyDataIndex;
                }
                var valueindexinfo = SearchConfigItemInfoIndex(keydataindex, name, false);
                return valueindexinfo != IndexInfo.Empty;
            }
        }

        #endregion

        #region Дополнительно

        protected override void Dispose(bool disposed)
        {
            if (disposed) _fstream.Dispose();
        }

        #endregion

        #endregion

        #region Свойства

        public override bool CanChangeBlocksCount
        {
            get {
                if (Disposed) throw new ObjectDisposedException(nameof(Config));
                lock (_syncroot) {
                    _fstream.Seek(11, SeekOrigin.Begin);
                    return _fstream.ReadByte() != 0;
                }
            }
        }

        public override int BlocksCount 
        {
            get {
                if (Disposed) throw new ObjectDisposedException(nameof(Config));
                lock (_syncroot) {
                    var buffer = new byte[2];
                    _fstream.Seek(12, SeekOrigin.Begin);
                    _fstream.Read(buffer, 0, buffer.Length);
                    return BitConverter.ToUInt16(buffer, 0);
                }
            }
        }

        public override int FreeBlocksCount 
        {
            get {
                if (Disposed) throw new ObjectDisposedException(nameof(Config));
                lock (_syncroot) {
                    using(var reader = new BinaryReader(_fstream, Encoding.UTF8, true)) {
                        _fstream.Seek(12, SeekOrigin.Begin);
                        var blockscount = reader.ReadUInt16();
                        var foundcount = 0;
                        for (var i = 1; i <= blockscount; ++i)
                            if (reader.ReadUInt16() == 0x0000) ++foundcount;
                        return foundcount;
                    }
                }
            }
        }

        public override int OccupiedBlocksCount
        {
            get {
                if (Disposed) throw new ObjectDisposedException(nameof(Config));
                lock (_syncroot) {
                    using (var reader = new BinaryReader(_fstream, Encoding.UTF8, true)) {
                        _fstream.Seek(12, SeekOrigin.Begin);
                        var blockscount = reader.ReadUInt16();
                        var foundcount = 0;
                        for (var i = 1; i <= blockscount; ++i)
                            if (reader.ReadUInt16() != 0x0000) ++foundcount;
                        return foundcount;
                    }
                }
            }
        }

        public override string FileName
        {
            get {
                if (Disposed) throw new ObjectDisposedException(nameof(Config));
                return _fstream.Name;
            }
        }

        public override int MaximumNestingDepth => 32;

        #endregion

    }
}
