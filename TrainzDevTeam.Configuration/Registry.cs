﻿using System;
using System.IO;
using System.Linq;
using System.Drawing;
using System.Collections.Generic;
using System.Globalization;

namespace TrainzDevTeam.Configuration
{
    /// <summary>
    /// Предосталвяет возможность организации реестра конфигурации
    /// </summary>
    public sealed class Registry : IDisposable
    {

        #region Данные класса

        private Dictionary<string, Config> _configurations;

        #endregion

        #region Конструктор

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="Registry"/>.
        /// </summary>
        public Registry() => _configurations = new Dictionary<string, Config>();

        #endregion

        #region Методы

        #region Статические

        /// <summary>
        /// Выполняет проверку, является ли указанный абсолютный путь к разделку конфигурации действительным.
        /// </summary>
        /// <param name="path">Полный путь к разделку конфигурации.</param>
        /// <returns>Значение true, если путь <paramref name="path"/> является действительным; в противном случае — значение false.</returns>
        public static bool RegistryKeyPathIsValid(string path) => Config.RelativeKeyPathIsValid(path);

        #endregion

        #region Управление структурой

        /// <summary>
        /// Включает в состав реестра файл конфигурации.
        /// </summary>
        /// <param name="registryKeyName">Наименвоание раздела реестра, который будет связан с подключаемым файлом.</param>
        /// <param name="path">Путь к файлу конфигурации реализующему раздел.</param>
        /// <returns>Объект <see cref="Config"/>, представляющий конфигурацию, связанную с файлом конфигурации, заданным в аргументе <paramref name="path"/> и разделом реестра <paramref name="registryKeyName"/></returns>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта реестра конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="registryKeyName"/> не может быть null. -или- Значение аргумента <paramref name="path"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="registryKeyName"/> содержит недопустимое значение. -или- <paramref name="path"/> представляет собой пустую строку (""), содержащую только пробел или хотябы один недопустимый символ. -или- <paramref name="path"/>ссылается на устройство не файлового типа, например "con:", "com1:", "lpt1:" и т. д., в среде NTFS.</exception>
        /// <exception cref="KeyAlreadyExistException">Раздел реестра <paramref name="registryKeyName"/> уже зарегистрирован.</exception>
        /// <exception cref="AlreadyIncludedException">Файл конфигурации <paramref name="path"/> уже зарегистрирован в реестре.</exception>
        /// <exception cref="System.NotSupportedException"><paramref name="path"/> ссылается на устройство не файлового типа, например "con:", "com1:", "lpt1:" и т. д., в среде, отличной от NTFS.</exception>
        /// <exception cref="FileNotFoundException">Не удается найти файл <paramref name="path"/>.</exception>
        /// <exception cref="IOException">Произошла ошибка ввода-вывода.</exception>
        /// <exception cref="DirectoryNotFoundException">Указан недопустимый путь (например, ведущий на несопоставленный диск).</exception>
        /// <exception cref="PathTooLongException">Указанный путь, имя файла или оба значения превышают максимальную длину, заданную в системе. Например, для платформ на основе Windows длина пути не должна превышать 248 знаков, а имен файлов — 260 знаков.</exception>
        /// <exception cref="WrongFileFormatException">Файл не является действительным файлом конфигурации.</exception>
        public Config IncludeFile(string registryKeyName, string path)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Registry));
            if (registryKeyName == null) throw new ArgumentNullException(nameof(registryKeyName));
            if (!Config.KeyOrValueNameIsValid(registryKeyName)) throw new ArgumentException(ResourceStrings.Exception_InvalidRegistryKeyName, nameof(registryKeyName));
            lock (_configurations) {
                if (_configurations.ContainsKey(registryKeyName.ToUpper(CultureInfo.CurrentCulture))) throw new KeyAlreadyExistException(registryKeyName);
                if(_configurations.Values.FirstOrDefault(config => string.Equals(config.FileName, path, StringComparison.CurrentCultureIgnoreCase)) != null) throw new AlreadyIncludedException(path);
                var configuration = Config.OpenFromConfigurationFile(path);
                configuration.Registry = this;
                _configurations.Add(registryKeyName.ToUpper(CultureInfo.CurrentCulture), configuration);
                return configuration;
            }
        }

        /// <summary>
        /// Исключает конфигурацию из реестра и вызывает <see cref="Config.Dispose()"/> для связанного с реестром объекта <see cref="Config"/>
        /// </summary>
        /// <param name="registryKeyName">Наименование раздела реестра, который необходимо исключить.</param>
        /// <param name="configLeaveOpen">Значение true, если нужно оставить исключённый файл конфигурации открытым (Функция <see cref="Config.Dispose()"/> не будет вызвана); в провтивном случае — значение false.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта реестра конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="registryKeyName"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="registryKeyName"/> содержит недопустимое значение.</exception>
        public void ExclusionFile(string registryKeyName, bool configLeaveOpen)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Registry));
            if (registryKeyName == null) throw new ArgumentNullException(nameof(registryKeyName));
            if (!Config.KeyOrValueNameIsValid(registryKeyName)) throw new ArgumentException(ResourceStrings.Exception_InvalidRegistryKeyName, nameof(registryKeyName));
            lock (_configurations) {
                var registrykeyname = registryKeyName.ToUpper(CultureInfo.CurrentCulture);
                if (_configurations.TryGetValue(registrykeyname, out var configuration)) {
                    configuration.Registry = null;
                    if (!configLeaveOpen) configuration.Dispose();
                    _configurations.Remove(registrykeyname);
                }
            }
        }

        /// <summary>
        /// Исключает конфигурацию из реестра и вызывает <see cref="Config.Dispose()"/> для связанного с реестром объекта <see cref="Config"/>
        /// </summary>
        /// <param name="registryKeyName">Наименование раздела реестра, который необходимо исключить.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта реестра конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="registryKeyName"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="registryKeyName"/> содержит недопустимое значение.</exception>
        public void ExclusionFile(string registryKeyName) => ExclusionFile(registryKeyName, false);

        #endregion

        #region Разделы

        /// <summary>
        /// Создаёт новый раздел конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделку конфмигурации, в котором необходимо создать подраздел.</param>
        /// <param name="keyName">Имя нового раздела конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта реестра конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="keyName"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="keyName"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Глубина вложенности для создаваемого раздела конфигурации превосходит допустимое значение (Смотри так же <seealso cref="Config.MaximumNestingDepth"/>).</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для создания раздела конфигурации (Смотри так же <seealso cref="Config.FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="KeyAlreadyExistException">Раздел конфигурации с именем M<paramref name="keyName"/> уже существует в разделе <paramref name="path"/>.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public void CreateKey(string path, string keyName)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Registry));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (!RegistryKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            var keynames = path.Trim('/').Split('/');
            lock (_configurations) {
                if (_configurations.TryGetValue(keynames[0].ToUpper(CultureInfo.CurrentCulture), out var configuration)) configuration.CreateKey((keynames.Length > 1) ? "/" + string.Join("/", keynames, 1, keynames.Length - 1) : "/", keyName);
                else throw new KeyNotFoundException(path.TrimEnd('/'));
            }
        }

        /// <summary>
        /// Переименовывает раздел конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделу конфигурации, который необходимо переименовать.</param>
        /// <param name="newKeyName">Новое наименование раздела конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта реестра конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="newKeyName"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="newKeyName"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="Config.MaximumNestingDepth"/>).</exception>
        /// <exception cref="InvalidOperationException">Невозможно переименовать корневой раздел конфигурации.</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="KeyAlreadyExistException">Раздел конфигурации с именем M<paramref name="newKeyName"/> уже существует в базовом разделе для <paramref name="path"/>.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public void RenameKey(string path, string newKeyName)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Registry));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (!RegistryKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            var keynames = path.Trim('/').Split('/');
            lock (_configurations) {
                if (_configurations.TryGetValue(keynames[0].ToUpper(CultureInfo.CurrentCulture), out var configuration)) configuration.RenameKey((keynames.Length > 1) ? "/" + string.Join("/", keynames, 1, keynames.Length - 1) : "/", newKeyName);
                else throw new KeyNotFoundException(path.TrimEnd('/'));
            }
        }

        /// <summary>
        /// Удаляет раздел конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделу конфигурации, который необходимо удалить.</param>
        /// <param name="recursive">Значение true, если нужно удалить все вложенные подразделы и параметры; в противном случае — значение false (будет выбрашено исключение <see cref="KeyNotEmptyException"/>, если удаляемый раздел не пустой).</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта реестра конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="Config.MaximumNestingDepth"/>).</exception>
        /// <exception cref="InvalidOperationException">Невозможно удалить корневой раздел конфигурации.</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="KeyNotEmptyException">Удаляемый раздел конфигурации не пустой (только если аргумент <paramref name="recursive"/> имеет значение false).</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        /// <remarks>Для удаления корневого раздела реестра конфигурации необходимо использовать метод <see cref="ExclusionFile(string)"/></remarks>
        public void DeleteKey(string path, bool recursive)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Registry));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (!RegistryKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            var keynames = path.Trim('/').Split('/');
            lock (_configurations) {
                if (_configurations.TryGetValue(keynames[0].ToUpper(CultureInfo.CurrentCulture), out var configuration)) configuration.DeleteKey((keynames.Length > 1) ? "/" + string.Join("/", keynames, 1, keynames.Length - 1) : "/", recursive);
                else throw new KeyNotFoundException(path.TrimEnd('/'));
            }
        }

        /// <summary>
        /// Удаляет раздел конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделу конфигурации, который необходимо удалить.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта реестра конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="Config.MaximumNestingDepth"/>).</exception>
        /// <exception cref="InvalidOperationException">Невозможно удалить корневой раздел конфигурации.</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="KeyNotEmptyException">Удаляемый раздел конфигурации не пустой.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        /// <remarks>Для удаления корневого раздела реестра конфигурации необходимо использовать метод <see cref="ExclusionFile(string)"/></remarks>
        public void DeleteKey(string path) => DeleteKey(path, false);

        /// <summary>
        /// Получает массив имён разделов конфигурации входящий в указанный раздел конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделу конфигурации для которого нужно получить список имён подразделов.</param>
        /// <returns>Массив <see cref="string"/>, содержащий список всех имён подразделов, указаного раздела конфигурации.</returns>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта реестра конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="Config.MaximumNestingDepth"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public string[] GetNameKeys(string path)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Registry));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (!RegistryKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            var keynames = path.Trim('/').Split('/');
            lock (_configurations) {
                if (_configurations.TryGetValue(keynames[0].ToUpper(CultureInfo.CurrentCulture), out var configuration)) return configuration.GetNameKeys((keynames.Length > 1) ? "/" + string.Join("/", keynames, 1, keynames.Length - 1) : "/");
                else throw new KeyNotFoundException(path.TrimEnd('/'));
            }
        }

        /// <summary>
        /// Выполняет проверку наличия раздела конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделу конфигурации, наличие которого необходимо проверить.</param>
        /// <returns>Значение true, если указанный раздел конфигурации существует; в противном случае — значение false.</returns>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта реестра конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="Config.MaximumNestingDepth"/>).</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public bool ExistKey(string path)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Registry));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (!RegistryKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            var keynames = path.Trim('/').Split('/');
            lock (_configurations) {
                if (_configurations.TryGetValue(keynames[0].ToUpper(CultureInfo.CurrentCulture), out var configuration)) return configuration.ExistKey((keynames.Length > 1) ? "/" + string.Join("/", keynames, 1, keynames.Length - 1) : "/");
                else throw new KeyNotFoundException(path.TrimEnd('/'));
            }
        }

        /// <summary>
        /// Создаёт объект <see cref="KeyInfo"/> для указаного раздела конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделу конфигурации.</param>
        /// <param name="createIfNotExist">Значение true, если нужно создать раздел конфигурации в случае его отсутствия; в противном случае — значение false.</param>
        /// <returns>Объект <see cref="KeyInfo"/> для указаного раздела конфигурации.</returns>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта реестра конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="Config.MaximumNestingDepth"/>).</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public KeyInfo OpenKey(string path, bool createIfNotExist)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Registry));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (!RegistryKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            var keynames = path.Trim('/').Split('/');
            lock (_configurations) {
                if (_configurations.TryGetValue(keynames[0].ToUpper(CultureInfo.CurrentCulture), out var configuration)) return configuration.OpenKey((keynames.Length > 1) ? "/" + string.Join("/", keynames, 1, keynames.Length - 1) : "/", createIfNotExist);
                else throw new KeyNotFoundException(path.TrimEnd('/'));
            }
        }

        /// <summary>
        /// Создаёт объект <see cref="KeyInfo"/> для указаного раздела конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделу конфигурации.</param>
        /// <returns>Объект <see cref="KeyInfo"/> для указаного раздела конфигурации.</returns>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта реестра конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="Config.MaximumNestingDepth"/>).</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public KeyInfo OpenKey(string path)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Registry));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (!RegistryKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            var keynames = path.Trim('/').Split('/');
            lock (_configurations) {
                if (_configurations.TryGetValue(keynames[0].ToUpper(CultureInfo.CurrentCulture), out var configuration)) return configuration.OpenKey((keynames.Length > 1) ? "/" + string.Join("/", keynames, 1, keynames.Length - 1) : "/");
                else throw new KeyNotFoundException(path.TrimEnd('/'));
            }
        }

        /// <summary>
        /// Получает раздел реестра, сопаставленный с указанной конфигурацией
        /// </summary>
        /// <param name="config">Объект <see cref="Config"/>, описывающий конфигурацию, для которой нужно определить раздел.</param>
        /// <returns>Строка, содержащая наименвоание раздела рееста, связано с конфигурацией <paramref name="config"/>; или значение null, если указанная конфигурация не связана с реестром.</returns>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта реестра конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="config"/> не может быть null.</exception>
        public string GetRegistryKeyName(Config config)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Registry));
            if (config == null) throw new ArgumentNullException(nameof(config));
            lock (_configurations) return _configurations.Where(pair => pair.Value == config).Select(pair => pair.Key).FirstOrDefault();
        }

        #endregion

        #region Параметры

        /// <summary>
        /// Создаёт новый или заменят уже существующий параметр конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта реестра конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null. -или- Значение аргумента <paramref name="value"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="Config.MaximumNestingDepth"/>).</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="Config.FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="TooLongDataException">Размер данных превоходит допустимое значение.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public void SetValue(string path, string name, byte[] value)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Registry));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (!RegistryKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            var keynames = path.Trim('/').Split('/');
            lock (_configurations) {
                if (_configurations.TryGetValue(keynames[0].ToUpper(CultureInfo.CurrentCulture), out var configuration)) configuration.SetValue((keynames.Length > 1) ? "/" + string.Join("/", keynames, 1, keynames.Length - 1) : "/", name, value);
                else throw new KeyNotFoundException(path.TrimEnd('/'));
            }
        }

        /// <summary>
        /// Создаёт новый или заменят уже существующий параметр конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта реестра конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="Config.MaximumNestingDepth"/>).</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="Config.FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public void SetValue(string path, string name, byte value)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Registry));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (!RegistryKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            var keynames = path.Trim('/').Split('/');
            lock (_configurations) {
                if (_configurations.TryGetValue(keynames[0].ToUpper(CultureInfo.CurrentCulture), out var configuration)) configuration.SetValue((keynames.Length > 1) ? "/" + string.Join("/", keynames, 1, keynames.Length - 1) : "/", name, value);
                else throw new KeyNotFoundException(path.TrimEnd('/'));
            }
        }

        /// <summary>
        /// Создаёт новый или заменят уже существующий параметр конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта реестра конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="Config.MaximumNestingDepth"/>).</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="Config.FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public void SetValue(string path, string name, short value)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Registry));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (!RegistryKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            var keynames = path.Trim('/').Split('/');
            lock (_configurations) {
                if (_configurations.TryGetValue(keynames[0].ToUpper(CultureInfo.CurrentCulture), out var configuration)) configuration.SetValue((keynames.Length > 1) ? "/" + string.Join("/", keynames, 1, keynames.Length - 1) : "/", name, value);
                else throw new KeyNotFoundException(path.TrimEnd('/'));
            }
        }

        /// <summary>
        /// Создаёт новый или заменят уже существующий параметр конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта реестра конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="Config.MaximumNestingDepth"/>).</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="Config.FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public void SetValue(string path, string name, int value)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Registry));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (!RegistryKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            var keynames = path.Trim('/').Split('/');
            lock (_configurations) {
                if (_configurations.TryGetValue(keynames[0].ToUpper(CultureInfo.CurrentCulture), out var configuration)) configuration.SetValue((keynames.Length > 1) ? "/" + string.Join("/", keynames, 1, keynames.Length - 1) : "/", name, value);
                else throw new KeyNotFoundException(path.TrimEnd('/'));
            }
        }

        /// <summary>
        /// Создаёт новый или заменят уже существующий параметр конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта реестра конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="Config.MaximumNestingDepth"/>).</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="Config.FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public void SetValue(string path, string name, long value)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Registry));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (!RegistryKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            var keynames = path.Trim('/').Split('/');
            lock (_configurations) {
                if (_configurations.TryGetValue(keynames[0].ToUpper(CultureInfo.CurrentCulture), out var configuration)) configuration.SetValue((keynames.Length > 1) ? "/" + string.Join("/", keynames, 1, keynames.Length - 1) : "/", name, value);
                else throw new KeyNotFoundException(path.TrimEnd('/'));
            }
        }

        /// <summary>
        /// Создаёт новый или заменят уже существующий параметр конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта реестра конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="Config.MaximumNestingDepth"/>).</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="Config.FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public void SetValue(string path, string name, ushort value)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Registry));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (!RegistryKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            var keynames = path.Trim('/').Split('/');
            lock (_configurations) {
                if (_configurations.TryGetValue(keynames[0].ToUpper(CultureInfo.CurrentCulture), out var configuration)) configuration.SetValue((keynames.Length > 1) ? "/" + string.Join("/", keynames, 1, keynames.Length - 1) : "/", name, value);
                else throw new KeyNotFoundException(path.TrimEnd('/'));
            }
        }

        /// <summary>
        /// Создаёт новый или заменят уже существующий параметр конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта реестра конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="Config.MaximumNestingDepth"/>).</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="Config.FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public void SetValue(string path, string name, uint value)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Registry));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (!RegistryKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            var keynames = path.Trim('/').Split('/');
            lock (_configurations) {
                if (_configurations.TryGetValue(keynames[0].ToUpper(CultureInfo.CurrentCulture), out var configuration)) configuration.SetValue((keynames.Length > 1) ? "/" + string.Join("/", keynames, 1, keynames.Length - 1) : "/", name, value);
                else throw new KeyNotFoundException(path.TrimEnd('/'));
            }
        }

        /// <summary>
        /// Создаёт новый или заменят уже существующий параметр конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта реестра конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="Config.MaximumNestingDepth"/>).</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="Config.FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public void SetValue(string path, string name, ulong value)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Registry));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (!RegistryKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            var keynames = path.Trim('/').Split('/');
            lock (_configurations) {
                if (_configurations.TryGetValue(keynames[0].ToUpper(CultureInfo.CurrentCulture), out var configuration)) configuration.SetValue((keynames.Length > 1) ? "/" + string.Join("/", keynames, 1, keynames.Length - 1) : "/", name, value);
                else throw new KeyNotFoundException(path.TrimEnd('/'));
            }
        }

        /// <summary>
        /// Создаёт новый или заменят уже существующий параметр конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта реестра конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="Config.MaximumNestingDepth"/>).</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="Config.FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public void SetValue(string path, string name, bool value)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Registry));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (!RegistryKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            var keynames = path.Trim('/').Split('/');
            lock (_configurations) {
                if (_configurations.TryGetValue(keynames[0].ToUpper(CultureInfo.CurrentCulture), out var configuration)) configuration.SetValue((keynames.Length > 1) ? "/" + string.Join("/", keynames, 1, keynames.Length - 1) : "/", name, value);
                else throw new KeyNotFoundException(path.TrimEnd('/'));
            }
        }

        /// <summary>
        /// Создаёт новый или заменят уже существующий параметр конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта реестра конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="Config.MaximumNestingDepth"/>).</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="Config.FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public void SetValue(string path, string name, decimal value)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Registry));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (!RegistryKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            var keynames = path.Trim('/').Split('/');
            lock (_configurations) {
                if (_configurations.TryGetValue(keynames[0].ToUpper(CultureInfo.CurrentCulture), out var configuration)) configuration.SetValue((keynames.Length > 1) ? "/" + string.Join("/", keynames, 1, keynames.Length - 1) : "/", name, value);
                else throw new KeyNotFoundException(path.TrimEnd('/'));
            }
        }

        /// <summary>
        /// Создаёт новый или заменят уже существующий параметр конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта реестра конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="Config.MaximumNestingDepth"/>).</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="Config.FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public void SetValue(string path, string name, double value)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Registry));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (!RegistryKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            var keynames = path.Trim('/').Split('/');
            lock (_configurations) {
                if (_configurations.TryGetValue(keynames[0].ToUpper(CultureInfo.CurrentCulture), out var configuration)) configuration.SetValue((keynames.Length > 1) ? "/" + string.Join("/", keynames, 1, keynames.Length - 1) : "/", name, value);
                else throw new KeyNotFoundException(path.TrimEnd('/'));
            }
        }

        /// <summary>
        /// Создаёт новый или заменят уже существующий параметр конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта реестра конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="Config.MaximumNestingDepth"/>).</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="Config.FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public void SetValue(string path, string name, sbyte value)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Registry));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (!RegistryKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            var keynames = path.Trim('/').Split('/');
            lock (_configurations) {
                if (_configurations.TryGetValue(keynames[0].ToUpper(CultureInfo.CurrentCulture), out var configuration)) configuration.SetValue((keynames.Length > 1) ? "/" + string.Join("/", keynames, 1, keynames.Length - 1) : "/", name, value);
                else throw new KeyNotFoundException(path.TrimEnd('/'));
            }
        }

        /// <summary>
        /// Создаёт новый или заменят уже существующий параметр конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта реестра конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="Config.MaximumNestingDepth"/>).</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="Config.FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public void SetValue(string path, string name, float value)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Registry));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (!RegistryKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            var keynames = path.Trim('/').Split('/');
            lock (_configurations) {
                if (_configurations.TryGetValue(keynames[0].ToUpper(CultureInfo.CurrentCulture), out var configuration)) configuration.SetValue((keynames.Length > 1) ? "/" + string.Join("/", keynames, 1, keynames.Length - 1) : "/", name, value);
                else throw new KeyNotFoundException(path.TrimEnd('/'));
            }
        }

        /// <summary>
        /// Создаёт новый или заменят уже существующий параметр конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта реестра конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null. -или- Значение аргумента <paramref name="value"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="Config.MaximumNestingDepth"/>).</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="Config.FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="TooLongDataException">Размер данных превоходит допустимое значение.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public void SetValue(string path, string name, string value)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Registry));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (!RegistryKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            var keynames = path.Trim('/').Split('/');
            lock (_configurations) {
                if (_configurations.TryGetValue(keynames[0].ToUpper(CultureInfo.CurrentCulture), out var configuration)) configuration.SetValue((keynames.Length > 1) ? "/" + string.Join("/", keynames, 1, keynames.Length - 1) : "/", name, value);
                else throw new KeyNotFoundException(path.TrimEnd('/'));
            }
        }

        /// <summary>
        /// Создаёт новый или заменят уже существующий параметр конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта реестра конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="Config.MaximumNestingDepth"/>).</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="Config.FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public void SetValue(string path, string name, Guid value)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Registry));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (!RegistryKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            var keynames = path.Trim('/').Split('/');
            lock (_configurations) {
                if (_configurations.TryGetValue(keynames[0].ToUpper(CultureInfo.CurrentCulture), out var configuration)) configuration.SetValue((keynames.Length > 1) ? "/" + string.Join("/", keynames, 1, keynames.Length - 1) : "/", name, value);
                else throw new KeyNotFoundException(path.TrimEnd('/'));
            }
        }

        /// <summary>
        /// Создаёт новый или заменят уже существующий параметр конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта реестра конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="Config.MaximumNestingDepth"/>).</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="Config.FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public void SetValue(string path, string name, Point value)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Registry));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (!RegistryKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            var keynames = path.Trim('/').Split('/');
            lock (_configurations) {
                if (_configurations.TryGetValue(keynames[0].ToUpper(CultureInfo.CurrentCulture), out var configuration)) configuration.SetValue((keynames.Length > 1) ? "/" + string.Join("/", keynames, 1, keynames.Length - 1) : "/", name, value);
                else throw new KeyNotFoundException(path.TrimEnd('/'));
            }
        }

        /// <summary>
        /// Создаёт новый или заменят уже существующий параметр конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта реестра конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="Config.MaximumNestingDepth"/>).</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="Config.FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public void SetValue(string path, string name, Size value)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Registry));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (!RegistryKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            var keynames = path.Trim('/').Split('/');
            lock (_configurations) {
                if (_configurations.TryGetValue(keynames[0].ToUpper(CultureInfo.CurrentCulture), out var configuration)) configuration.SetValue((keynames.Length > 1) ? "/" + string.Join("/", keynames, 1, keynames.Length - 1) : "/", name, value);
                else throw new KeyNotFoundException(path.TrimEnd('/'));
            }
        }

        /// <summary>
        /// Создаёт новый или заменят уже существующий параметр конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта реестра конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null. -или- Значение аргумента <paramref name="value"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="Config.MaximumNestingDepth"/>).</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="Config.FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public void SetValue(string path, string name, Version value)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Registry));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (!RegistryKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            var keynames = path.Trim('/').Split('/');
            lock (_configurations) {
                if (_configurations.TryGetValue(keynames[0].ToUpper(CultureInfo.CurrentCulture), out var configuration)) configuration.SetValue((keynames.Length > 1) ? "/" + string.Join("/", keynames, 1, keynames.Length - 1) : "/", name, value);
                else throw new KeyNotFoundException(path.TrimEnd('/'));
            }
        }

        /// <summary>
        /// Получает массив имён параметров конфигурации входящий в указанный раздел конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделу конфигурации для которого нужно получить список имён параметров.</param>
        /// <returns>Массив <see cref="string"/>, содержащий список всех имён параметров, указаного раздела конфигурации.</returns>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта реестра конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="Config.MaximumNestingDepth"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public string[] GetNameValues(string path)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Registry));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (!RegistryKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            var keynames = path.Trim('/').Split('/');
            lock (_configurations) {
                if (_configurations.TryGetValue(keynames[0].ToUpper(CultureInfo.CurrentCulture), out var configuration)) return configuration.GetNameValues((keynames.Length > 1) ? "/" + string.Join("/", keynames, 1, keynames.Length - 1) : "/");
                else throw new KeyNotFoundException(path.TrimEnd('/'));
            }
        }

        /// <summary>
        /// Получает массив параметров конфигурации, входящих в указанный раздел конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделу конфигурации для которого нужно получить список параметров.</param>
        /// <returns>Массив <see cref="ValueInfo"/>, содержащий список всех параметров, указаного раздела конфигурации.</returns>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта реестра конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="Config.MaximumNestingDepth"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public ValueInfo[] GetInfoValues(string path)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Registry));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (!RegistryKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            var keynames = path.Trim('/').Split('/');
            lock (_configurations) {
                if (_configurations.TryGetValue(keynames[0].ToUpper(CultureInfo.CurrentCulture), out var configuration)) return configuration.GetInfoValues((keynames.Length > 1) ? "/" + string.Join("/", keynames, 1, keynames.Length - 1) : "/");
                else throw new KeyNotFoundException(path.TrimEnd('/'));
            }
        }

        /// <summary>
        /// Переименовывает параметр конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="newName">Новое наименование параметра конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта реестра конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null. -или- Значение аргумента <paramref name="newName"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение. -или- Аргумент <paramref name="newName"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="Config.MaximumNestingDepth"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="ValueNotFoundException">Раздел конфигурации <paramref name="name"/> не существует в разделе <paramref name="path"/>.</exception>
        /// <exception cref="ValueAlreadyExistException">Раздел конфигурации с именем <paramref name="newName"/> уже присутствует в разделе <paramref name="path"/>.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public void RenameValue(string path, string name, string newName)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Registry));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (!RegistryKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            var keynames = path.Trim('/').Split('/');
            lock (_configurations) {
                if (_configurations.TryGetValue(keynames[0].ToUpper(CultureInfo.CurrentCulture), out var configuration)) configuration.RenameValue((keynames.Length > 1) ? "/" + string.Join("/", keynames, 1, keynames.Length - 1) : "/", name, newName);
                else throw new KeyNotFoundException(path.TrimEnd('/'));
            }
        }

        /// <summary>
        /// Возвращает параметр конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <returns>Объект <see cref="ValueInfo"/>, описывающий параметр конфигурации.</returns>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта реестра конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="Config.MaximumNestingDepth"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="ValueNotFoundException">Раздел конфигурации <paramref name="name"/> не существует в разделе <paramref name="path"/>.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public ValueInfo GetValueInfo(string path, string name)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Registry));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (!RegistryKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            var keynames = path.Trim('/').Split('/');
            lock (_configurations) {
                if (_configurations.TryGetValue(keynames[0].ToUpper(CultureInfo.CurrentCulture), out var configuration)) return configuration.GetValueInfo((keynames.Length > 1) ? "/" + string.Join("/", keynames, 1, keynames.Length - 1) : "/", name);
                else throw new KeyNotFoundException(path.TrimEnd('/'));
            }
        }

        /// <summary>
        /// Получает значение параметра конфигурации с указанием значения по умолчанию и необходимостью проверять тип параметра конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="defaultValue">Значение по умолчанию.</param>
        /// <param name="checkType">Значение true, если тип данных значение по умолчанию <paramref name="defaultValue"/> не будет совпадать с типом данных параметра, то будет возвращено значение по умолчанию <paramref name="defaultValue"/>; если значение false, значение по умолчанию <paramref name="defaultValue"/> будет возвращено только если параметр отсутствует.</param>
        /// <returns>Значение параметра или значение по умолчанию <paramref name="defaultValue"/> если параметр отсутствует или его тип не совпадает с ожидаемым при включённгой проверке типов <paramref name="checkType"/></returns>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта реестра конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null. -или- Значение аргумента <paramref name="defaultValue"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="Config.MaximumNestingDepth"/>).</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public object GetValue(string path, string name, object defaultValue, bool checkType)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Registry));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (!RegistryKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            var keynames = path.Trim('/').Split('/');
            lock (_configurations) {
                if (_configurations.TryGetValue(keynames[0].ToUpper(CultureInfo.CurrentCulture), out var configuration)) return configuration.GetValue((keynames.Length > 1) ? "/" + string.Join("/", keynames, 1, keynames.Length - 1) : "/", name, defaultValue, checkType);
                else throw new KeyNotFoundException(path.TrimEnd('/'));
            }
        }

        /// <summary>
        /// Получение значения параметра конфигурации с указанием значения по умолчанию и проверкой типа параметра конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="defaultValue">Значение по умолчанию.</param>
        /// <returns>Значение параметра или значение по умолчанию <paramref name="defaultValue"/> если параметр отсутствует или его тип не соответствует типу значения по умолчанию.</returns>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта реестра конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null. -или- Значение аргумента <paramref name="defaultValue"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="Config.MaximumNestingDepth"/>).</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public object GetValue(string path, string name, object defaultValue) => GetValue(path, name, defaultValue, true);

        /// <summary>
        /// Получение значения параметра.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <returns>Значение параметра конфигурации.</returns>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта реестра конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="Config.MaximumNestingDepth"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="ValueNotFoundException">Раздел конфигурации <paramref name="name"/> не существует в разделе <paramref name="path"/>.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public object GetValue(string path, string name)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Registry));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (!RegistryKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            var keynames = path.Trim('/').Split('/');
            lock (_configurations) {
                if (_configurations.TryGetValue(keynames[0].ToUpper(CultureInfo.CurrentCulture), out var configuration)) return configuration.GetValue((keynames.Length > 1) ? "/" + string.Join("/", keynames, 1, keynames.Length - 1) : "/", name);
                else throw new KeyNotFoundException(path.TrimEnd('/'));
            }
        }

        /// <summary>
        /// Получение значения параметра как строки.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="defaultValue">Значение по умолчанию.</param>
        /// <param name="canConvertType">Значение true, если нужно преобразовать значение другого типа к строке; значение false — будет возвращено значение по умолчанию <paramref name="defaultValue"/>, для несоответствующего типа параметра.</param>
        /// <returns>Значение параметра или значение по умолчанию <paramref name="defaultValue"/> если прараметр отсутствует или имеет не строковый тип с отключённой конвертацией <paramref name="canConvertType"/></returns>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта реестра конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="Config.MaximumNestingDepth"/>).</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public string GetValueAsString(string path, string name, string defaultValue = null, bool canConvertType = true)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Registry));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (!RegistryKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            var keynames = path.Trim('/').Split('/');
            lock (_configurations) {
                if (_configurations.TryGetValue(keynames[0].ToUpper(CultureInfo.CurrentCulture), out var configuration)) return configuration.GetValueAsString((keynames.Length > 1) ? "/" + string.Join("/", keynames, 1, keynames.Length - 1) : "/", name, defaultValue, canConvertType);
                else throw new KeyNotFoundException(path.TrimEnd('/'));
            }
        }

        /// <summary>
        /// Получение значения параметра как логическое знчаение.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="defaultValue">Значение по умолчанию.</param>
        /// <param name="canConvertType">Значение true, если нужно преобразовать значение другого типа к строке; значение false — будет возвращено значение по умолчанию <paramref name="defaultValue"/>, для несоответствующего типа параметра.</param>
        /// <returns>Значение параметра или значение по умолчанию <paramref name="defaultValue"/> если прараметр отсутствует или имеет не строковый тип с отключённой конвертацией <paramref name="canConvertType"/></returns>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта реестра конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="Config.MaximumNestingDepth"/>).</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public bool GetValueAsBool(string path, string name, bool defaultValue = false, bool canConvertType = true)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Registry));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (!RegistryKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            var keynames = path.Trim('/').Split('/');
            lock (_configurations) {
                if (_configurations.TryGetValue(keynames[0].ToUpper(CultureInfo.CurrentCulture), out var configuration)) return configuration.GetValueAsBool((keynames.Length > 1) ? "/" + string.Join("/", keynames, 1, keynames.Length - 1) : "/", name, defaultValue, canConvertType);
                else throw new KeyNotFoundException(path.TrimEnd('/'));
            }
        }

        /// <summary>
        /// Получение значения параметра как <see cref="Guid"/>.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="defaultValue">Значение по умолчанию.</param>
        /// <param name="canConvertType">Значение true, если нужно преобразовать значение другого типа к строке; значение false — будет возвращено значение по умолчанию <paramref name="defaultValue"/>, для несоответствующего типа параметра.</param>
        /// <returns>Значение параметра или значение по умолчанию <paramref name="defaultValue"/> если прараметр отсутствует или имеет не строковый тип с отключённой конвертацией <paramref name="canConvertType"/></returns>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта реестра конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="Config.MaximumNestingDepth"/>).</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public Guid GetValueAsGuid(string path, string name, Guid defaultValue = default, bool canConvertType = true)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Registry));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (!RegistryKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            var keynames = path.Trim('/').Split('/');
            lock (_configurations) {
                if (_configurations.TryGetValue(keynames[0].ToUpper(CultureInfo.CurrentCulture), out var configuration)) return configuration.GetValueAsGuid((keynames.Length > 1) ? "/" + string.Join("/", keynames, 1, keynames.Length - 1) : "/", name, defaultValue, canConvertType);
                else throw new KeyNotFoundException(path.TrimEnd('/'));
            }
        }

        /// <summary>
        /// Получение значения параметра как <see cref="Version"/>
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="defaultValue">Значение по умолчанию</param>
        /// <param name="canConvertType">Значение true, если нужно преобразовать значение другого типа к строке; значение false — будет возвращено значение по умолчанию <paramref name="defaultValue"/>, для несоответствующего типа параметра.</param>
        /// <returns>Значение параметра или значение по умолчанию <paramref name="defaultValue"/> если прараметр отсутствует или имеет не строковый тип с отключённой конвертацией <paramref name="canConvertType"/></returns>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта реестра конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="Config.MaximumNestingDepth"/>).</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public Version GetValueAsVersion(string path, string name, Version defaultValue = null, bool canConvertType = true)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Registry));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (!RegistryKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            var keynames = path.Trim('/').Split('/');
            lock (_configurations) {
                if (_configurations.TryGetValue(keynames[0].ToUpper(CultureInfo.CurrentCulture), out var configuration)) return configuration.GetValueAsVersion((keynames.Length > 1) ? "/" + string.Join("/", keynames, 1, keynames.Length - 1) : "/", name, defaultValue, canConvertType);
                else throw new KeyNotFoundException(path.TrimEnd('/'));
            }
        }

        /// <summary>
        /// Получение значения параметра конфигурации с указанным типом значения.
        /// </summary>
        /// <typeparam name="TValue">Тип возвращаемого значения.</typeparam>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="defaultValue">Значение по умолчанию.</param>
        /// <returns>Значение параметра или значение по умолчанию <paramref name="defaultValue"/> если параметр отсутствует или его тип не соответствует типу значения по умолчанию.</returns>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта реестра конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="Config.MaximumNestingDepth"/>).</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public TValue GetValueAs<TValue>(string path, string name, TValue defaultValue = default)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Registry));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (!RegistryKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            var keynames = path.Trim('/').Split('/');
            lock (_configurations) {
                if (_configurations.TryGetValue(keynames[0].ToUpper(CultureInfo.CurrentCulture), out var configuration)) return configuration.GetValueAs((keynames.Length > 1) ? "/" + string.Join("/", keynames, 1, keynames.Length - 1) : "/", name, defaultValue);
                else throw new KeyNotFoundException(path.TrimEnd('/'));
            }
        }

        /// <summary>
        /// Удаление параметра конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта реестра конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="Config.MaximumNestingDepth"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="ValueNotFoundException">Раздел конфигурации <paramref name="name"/> не существует в разделе <paramref name="path"/>.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public void DeleteValue(string path, string name)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Registry));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (!RegistryKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            var keynames = path.Trim('/').Split('/');
            lock (_configurations) {
                if (_configurations.TryGetValue(keynames[0].ToUpper(CultureInfo.CurrentCulture), out var configuration)) configuration.DeleteValue((keynames.Length > 1) ? "/" + string.Join("/", keynames, 1, keynames.Length - 1) : "/", name);
                else throw new KeyNotFoundException(path.TrimEnd('/'));
            }
        }

        /// <summary>
        /// Выполняет проверку наличия параметра конфигурации.
        /// </summary>
        /// <param name="path">Путь к разделку, в котором распологается параметр конфигурации.</param>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <returns>Значение true, если параметр конфигурации <paramref name="name"/> существует в разделе конфигурации <paramref name="path"/>; в противном случае — значение false.</returns>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта реестра конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null. -или- Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы. -или- Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="Config.MaximumNestingDepth"/>).</exception>
        /// <exception cref="KeyNotFoundException">Раздел конфигурации, указанный в <paramref name="path"/> не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public bool ExistValue(string path, string name)
        {
            if (Disposed) throw new ObjectDisposedException(nameof(Registry));
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (!RegistryKeyPathIsValid(path)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyPath, nameof(path));
            var keynames = path.Trim('/').Split('/');
            lock (_configurations) {
                if (_configurations.TryGetValue(keynames[0].ToUpper(CultureInfo.CurrentCulture), out var configuration)) return configuration.ExistValue((keynames.Length > 1) ? "/" + string.Join("/", keynames, 1, keynames.Length - 1) : "/", name);
                else throw new KeyNotFoundException(path.TrimEnd('/'));
            }
        }

        #endregion

        #region Дополнительно

        /// <summary>
        /// Освобождает все ресурсы, занятые текущим объектом <see cref="Registry"/>
        /// </summary>
        public void Dispose()
        {
            if (!Disposed) {
                foreach (Config config in _configurations.Values) {
                    config.Registry = null;
                    config.Dispose();
                }
                Disposed = true;
            }
        }

        #endregion

        #endregion

        #region Операторы

        /// <summary>
        /// Создаёт объект <see cref="KeyInfo"/> для указанного раздела.
        /// </summary>
        /// <param name="registry">Реестр, для которого необходимо открыть раздел конфигурации</param>
        /// <param name="path">Путь к разделу конфигурации.</param>
        /// <returns>Объект <see cref="KeyInfo"/> для указаного раздела конфигурации.</returns>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта реестра конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="path"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> имеет неверный формат или содержит недопустимые символы.</exception>
        /// <exception cref="NestingDepthExceededException">Путь к разделу конфигурации <paramref name="path"/> превосходит глубину вложенности (Смотри так же <seealso cref="Config.MaximumNestingDepth"/>).</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Usage", "CA2225")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1062")]
        public static KeyInfo operator +(Registry registry, string path) => registry.OpenKey(path);

        #endregion

        #region Свойства

        /// <summary>
        /// Возаращает значение true, если все ресурсы для этого объекта реестра были освобождены; в противном случае — значение false.
        /// </summary>
        public bool Disposed { get; private set; }

        /// <summary>
        /// Возвращает список имён корневых разделов реестра.
        /// </summary>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта реестра конфигурации были освобождены.</exception>
        public IReadOnlyList<string> NameRootKeys
        {
            get {
                if (Disposed) throw new ObjectDisposedException(nameof(Registry));
                lock (_configurations) return _configurations.Select(pair => pair.Key).ToArray();
            }
        }

        /// <summary>
        /// Возвращает количество корневх разделов реестра конфигурации.
        /// </summary>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта реестра конфигурации были освобождены.</exception>
        public int RootKeyCount
        {
            get {
                if (Disposed) throw new ObjectDisposedException(nameof(Registry));
                return _configurations.Count;
            }
        }

        #endregion

    }
}
