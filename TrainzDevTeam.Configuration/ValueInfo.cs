﻿using System;

namespace TrainzDevTeam.Configuration
{
    /// <summary>
    /// Описывает информацию о параметре конфигурации.
    /// </summary>
    public struct ValueInfo : IEquatable<ValueInfo> 
    {

        #region Конструктор

        internal ValueInfo(string path, string name, object value)
        {
            Path = path;
            Name = name;
            Value = value;
        }

        #endregion

        #region Методы

        /// <summary>
        /// Сравниывает указанный объект <see cref="ValueInfo"/> с текущим объектом <see cref="ValueInfo"/>.
        /// </summary>
        /// <param name="info">Объект <see cref="ValueInfo"/>, который нужно сравнить.</param>
        /// <returns>Значение true, если объект <paramref name="info"/> равен текущему; в противном случае — значение false.</returns>
        public bool Equals(ValueInfo info) => string.Equals(Path, info.Path, StringComparison.CurrentCultureIgnoreCase) && string.Equals(Name, info.Name, StringComparison.CurrentCultureIgnoreCase) && ((ValueIsError && info.ValueIsError) || (!ValueIsError && !info.ValueIsError && Value.Equals(info.Value)));

        public override bool Equals(object obj) => obj != null && obj is ValueInfo && Equals((ValueInfo)obj);

        public override int GetHashCode() => (Path?.GetHashCode() ?? 0) ^ (Name?.GetHashCode() ?? 0) ^ (Value?.GetHashCode() ?? 0);

        #endregion

        #region Свойства

        /// <summary>
        /// Получает путь к разделу конфигурации, в котором находится текущий параметр конфигурации.
        /// </summary>
        public string Path { get; }

        /// <summary>
        /// Получает наименование параметра конфигурации.
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Получает значение параметра конфигурации.
        /// </summary>
        public object Value { get; }

        /// <summary>
        /// Возвращает значение true, если текущий параметр конфигурации содержит ошибку; в противном случае — значение false.
        /// </summary>
        public bool ValueIsError => Value == null;

        /// <summary>
        /// Возвращает объект <see cref="System.Type"/>, содержащий тип данных для значения параметра конфигурации; или значение null, если параметр содержит ошибку.
        /// </summary>
        public Type Type => Value?.GetType();

        #endregion

        #region Операторы

        public static bool operator ==(ValueInfo left, ValueInfo right) => left.Equals(right);

        public static bool operator !=(ValueInfo left, ValueInfo right) => !left.Equals(right);

        #endregion

    }

}
