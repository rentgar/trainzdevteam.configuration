﻿using System;
using System.IO;
using System.Linq;
using System.Drawing;
using System.Collections.Generic;
using System.Globalization;

namespace TrainzDevTeam.Configuration
{

    /// <summary>
    /// Описывает раздел конфигурации.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1036", Justification = "<Ожидание>")]
    public sealed class KeyInfo : IEquatable<KeyInfo>, IEquatable<string>, IComparable<KeyInfo>, IComparable<string>
    {

        #region Конструктор

        internal KeyInfo(Config configuration, string path)
        {
            Configuration = configuration;
            Path = path;
        }

        #endregion

        #region Методы

        #region Разделы

        /// <summary>
        /// Создаёт раздел кофигурайции, представленный текущий объектом <see cref="KeyInfo"/>
        /// </summary>
        /// <exception cref="ObjectDisposedException">Все ресурсы для объекта конфигурации <see cref="Configuration"/> были освобождены.</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для создания раздела конфигурации (Смотри так же <seealso cref="Config.FreeBlocksCount"/>).</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public void Create()
        {
            if (Path == "/") return;
            var keynames = Path.Trim('/').Split('/');
            var keypath = "/";
            foreach (var keyname in keynames) {
                if (!Configuration.ExistKey(keypath + keyname))
                    Configuration.CreateKey(keypath, keyname);
                keypath += keyname + "/";
            }
        }

        /// <summary>
        /// Создаёт новый подраздел конфигурации в текущем разделе конфигурации.
        /// </summary>
        /// <param name="keyName">Наименвоание нового раздела конфигурации.</param>
        /// <returns>Объект <see cref="KeyInfo"/>, представляющий созданный подраздел.</returns>
        /// <exception cref="ObjectDisposedException">Все ресурсы для объекта конфигурации <see cref="Configuration"/> были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="keyName"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="keyName"/> содержит недопустимое значение.</exception>
        /// <exception cref="NestingDepthExceededException">Глубина вложенности для создаваемого раздела конфигурации превосходит допустимое значение (Смотри так же <seealso cref="Config.MaximumNestingDepth"/>).</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для создания раздела конфигурации (Смотри так же <seealso cref="Config.FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Текущий рздел конфигурации не существует.</exception>
        /// <exception cref="KeyAlreadyExistException">Раздел конфигурации с именем <paramref name="keyName"/> уже существует в текущем разделе.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public KeyInfo Create(string keyName)
        {
            Configuration.CreateKey(Path, keyName);
            return new KeyInfo(Configuration, Config.CombinePath(Path, keyName));
        }

        /// <summary>
        /// Переименовывает текущий раздел конфигурации
        /// </summary>
        /// <param name="newKeyName">Новое имя раздела</param>
        /// <returns>Объект <see cref="KeyInfo"/>, представляющий переименованный раздел конфигурации</returns>
        /// <exception cref="ObjectDisposedException">Все ресурсы для объекта конфигурации <see cref="Configuration"/> были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="newKeyName"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="newKeyName"/> содержит недопустимое значение.</exception>
        /// <exception cref="InvalidOperationException">Невозможно переименовать корневой раздел конфигурации.</exception>
        /// <exception cref="KeyNotFoundException">Текущий рздел конфигурации не существует.</exception>
        /// <exception cref="KeyAlreadyExistException">Раздел конфигурации с именем <paramref name="newKeyName"/> уже существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public KeyInfo Rename(string newKeyName)
        {
            Configuration.RenameKey(Path, newKeyName);
            var keynames = Path.Trim('/').Split('/');
            var basekeypath = (keynames.Length > 1) ? string.Join("/", keynames, 0, keynames.Length - 1) + '/' : string.Empty;
            return new KeyInfo(Configuration, $"/{basekeypath}{newKeyName}");
        }

        /// <summary>
        /// Удаляет текущий раздел конфгурации
        /// </summary>
        /// <param name="recursive">Значение true, если нужно удалить все вложенные подразделы и параметры; в противном случае — значение false (будет выбрашено исключение <see cref="KeyNotEmptyException"/>, если удаляемый раздел не пустой).</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для объекта конфигурации <see cref="Configuration"/> были освобождены.</exception>
        /// <exception cref="InvalidOperationException">Невозможно удалить корневой раздел конфигурации.</exception>
        /// <exception cref="KeyNotFoundException">Текущий рздел конфигурации не существует.</exception>
        /// <exception cref="KeyNotEmptyException">Текущий раздел конфигурации не пустой (только если аргумент <paramref name="recursive"/> имеет значение false).</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public void Delete(bool recursive) => Configuration.DeleteKey(Path, recursive);

        /// <summary>
        /// Удаляет текущий раздел конфгурации
        /// </summary>
        /// <exception cref="ObjectDisposedException">Все ресурсы для объекта конфигурации <see cref="Configuration"/> были освобождены.</exception>
        /// <exception cref="InvalidOperationException">Невозможно удалить корневой раздел конфигурации.</exception>
        /// <exception cref="KeyNotFoundException">Текущий рздел конфигурации не существует.</exception>
        /// <exception cref="KeyNotEmptyException">Текущий раздел конфигурации не пустой.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public void Delete() => Configuration.DeleteKey(Path, false);

        /// <summary>
        /// Создаёт объект <see cref="KeyInfo"/> для указаного раздела конфигурации.
        /// </summary>
        /// <param name="relativePath">Относительный путь к подразделу конфигурации.</param>
        /// <returns>Объект <see cref="KeyInfo"/> для указаного раздела конфигурации.</returns>
        /// <exception cref="ObjectDisposedException">Все ресурсы для объекта конфигурации <see cref="Configuration"/> были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="relativePath"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="relativePath"/> имеет неверный формат или содержит недопустимые символы.</exception>
        /// <exception cref="NestingDepthExceededException">Глубина вложенности для создаваемого раздела конфигурации превосходит допустимое значение (Смотри так же <seealso cref="Config.MaximumNestingDepth"/>).</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public KeyInfo OpenSubKey(string relativePath)
        {
            if (Configuration.Disposed) throw new ObjectDisposedException(nameof(Config));
            if (relativePath == null) throw new ArgumentNullException(nameof(relativePath));
            if (!Config.RelativeKeyPathIsValid(relativePath)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyRelativePath, nameof(relativePath));
            return Configuration.OpenKey(Config.CombinePath(Path, relativePath.TrimEnd('/')));
        }

        /// <summary>
        /// Создаёт объект <see cref="KeyInfo"/> для указаного раздела конфигурации.
        /// </summary>
        /// <param name="relativePath">Относительный путь к подразделу конфигурации.</param>
        /// <param name="createIfNotExist">Значение true, если нужно создать раздел конфигурации в случае его отсутствия; в противном случае — значение false.</param>
        /// <returns>Объект <see cref="KeyInfo"/> для указаного раздела конфигурации.</returns>
        /// <exception cref="ObjectDisposedException">Все ресурсы для объекта конфигурации <see cref="Configuration"/> были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="relativePath"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="relativePath"/> имеет неверный формат или содержит недопустимые символы.</exception>
        /// <exception cref="NestingDepthExceededException">Глубина вложенности для создаваемого раздела конфигурации превосходит допустимое значение (Смотри так же <seealso cref="Config.MaximumNestingDepth"/>).</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public KeyInfo OpenSubKey(string relativePath, bool createIfNotExist)
        {
            if (Configuration.Disposed) throw new ObjectDisposedException(nameof(Config));
            if (relativePath == null) throw new ArgumentNullException(nameof(relativePath));
            if (!Config.RelativeKeyPathIsValid(relativePath)) throw new ArgumentException(ResourceStrings.Exception_InvalidKeyRelativePath, nameof(relativePath));
            return Configuration.OpenKey(Config.CombinePath(Path, relativePath.TrimEnd('/')), createIfNotExist);
        }

        #endregion

        #region Параметры

        /// <summary>
        /// Создает или заменяет уже существующий праметр в разделе, представленным текущим <see cref="KeyInfo"/>.
        /// </summary>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для объекта конфигурации <see cref="Configuration"/> были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="name"/> не может быть null. -или- Значение аргумента <paramref name="value"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="Config.FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Текущий рздел конфигурации не существует.</exception>
        /// <exception cref="TooLongDataException">Размер данных превоходит допустимое значение.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public void SetValue(string name, byte[] value) => Configuration.SetValue(Path, name, value);

        /// <summary>
        /// Создает или заменяет уже существующий праметр в разделе, представленным текущим <see cref="KeyInfo"/>.
        /// </summary>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для объекта конфигурации <see cref="Configuration"/> были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="Config.FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Текущий рздел конфигурации не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public void SetValue(string name, byte value) => Configuration.SetValue(Path, name, value);

        /// <summary>
        /// Создает или заменяет уже существующий праметр в разделе, представленным текущим <see cref="KeyInfo"/>.
        /// </summary>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для объекта конфигурации <see cref="Configuration"/> были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="Config.FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Текущий рздел конфигурации не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public void SetValue(string name, short value) => Configuration.SetValue(Path, name, value);

        /// <summary>
        /// Создает или заменяет уже существующий праметр в разделе, представленным текущим <see cref="KeyInfo"/>.
        /// </summary>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для объекта конфигурации <see cref="Configuration"/> были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="Config.FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Текущий рздел конфигурации не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public void SetValue(string name, int value) => Configuration.SetValue(Path, name, value);

        /// <summary>
        /// Создает или заменяет уже существующий праметр в разделе, представленным текущим <see cref="KeyInfo"/>.
        /// </summary>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для объекта конфигурации <see cref="Configuration"/> были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="Config.FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Текущий рздел конфигурации не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public void SetValue(string name, long value) => Configuration.SetValue(Path, name, value);

        /// <summary>
        /// Создает или заменяет уже существующий праметр в разделе, представленным текущим <see cref="KeyInfo"/>.
        /// </summary>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для объекта конфигурации <see cref="Configuration"/> были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="Config.FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Текущий рздел конфигурации не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public void SetValue(string name, ushort value) => Configuration.SetValue(Path, name, value);

        /// <summary>
        /// Создает или заменяет уже существующий праметр в разделе, представленным текущим <see cref="KeyInfo"/>.
        /// </summary>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для объекта конфигурации <see cref="Configuration"/> были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="Config.FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Текущий рздел конфигурации не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public void SetValue(string name, uint value) => Configuration.SetValue(Path, name, value);

        /// <summary>
        /// Создает или заменяет уже существующий праметр в разделе, представленным текущим <see cref="KeyInfo"/>.
        /// </summary>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для объекта конфигурации <see cref="Configuration"/> были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="Config.FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Текущий рздел конфигурации не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public void SetValue(string name, ulong value) => Configuration.SetValue(Path, name, value);

        /// <summary>
        /// Создает или заменяет уже существующий праметр в разделе, представленным текущим <see cref="KeyInfo"/>.
        /// </summary>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для объекта конфигурации <see cref="Configuration"/> были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="Config.FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Текущий рздел конфигурации не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public void SetValue(string name, bool value) => Configuration.SetValue(Path, name, value);

        /// <summary>
        /// Создает или заменяет уже существующий праметр в разделе, представленным текущим <see cref="KeyInfo"/>.
        /// </summary>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для объекта конфигурации <see cref="Configuration"/> были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="Config.FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Текущий рздел конфигурации не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public void SetValue(string name, decimal value) => Configuration.SetValue(Path, name, value);

        /// <summary>
        /// Создает или заменяет уже существующий праметр в разделе, представленным текущим <see cref="KeyInfo"/>.
        /// </summary>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для объекта конфигурации <see cref="Configuration"/> были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="Config.FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Текущий рздел конфигурации не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public void SetValue(string name, double value) => Configuration.SetValue(Path, name, value);

        /// <summary>
        /// Создает или заменяет уже существующий праметр в разделе, представленным текущим <see cref="KeyInfo"/>.
        /// </summary>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для объекта конфигурации <see cref="Configuration"/> были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="Config.FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Текущий рздел конфигурации не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public void SetValue(string name, sbyte value) => Configuration.SetValue(Path, name, value);

        /// <summary>
        /// Создает или заменяет уже существующий праметр в разделе, представленным текущим <see cref="KeyInfo"/>.
        /// </summary>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для объекта конфигурации <see cref="Configuration"/> были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="Config.FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Текущий рздел конфигурации не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public void SetValue(string name, float value) => Configuration.SetValue(Path, name, value);

        /// <summary>
        /// Создает или заменяет уже существующий праметр в разделе, представленным текущим <see cref="KeyInfo"/>.
        /// </summary>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для объекта конфигурации <see cref="Configuration"/> были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="name"/> не может быть null. -или- Значение аргумента <paramref name="value"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="Config.FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Текущий рздел конфигурации не существует.</exception>
        /// <exception cref="TooLongDataException">Размер данных превоходит допустимое значение.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public void SetValue(string name, string value) => Configuration.SetValue(Path, name, value);

        /// <summary>
        /// Создает или заменяет уже существующий праметр в разделе, представленным текущим <see cref="KeyInfo"/>.
        /// </summary>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для объекта конфигурации <see cref="Configuration"/> были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="Config.FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Текущий рздел конфигурации не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public void SetValue(string name, Guid value) => Configuration.SetValue(Path, name, value);

        /// <summary>
        /// Создает или заменяет уже существующий праметр в разделе, представленным текущим <see cref="KeyInfo"/>.
        /// </summary>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для объекта конфигурации <see cref="Configuration"/> были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="Config.FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Текущий рздел конфигурации не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public void SetValue(string name, Point value) => Configuration.SetValue(Path, name, value);

        /// <summary>
        /// Создает или заменяет уже существующий праметр в разделе, представленным текущим <see cref="KeyInfo"/>.
        /// </summary>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для объекта конфигурации <see cref="Configuration"/> были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="Config.FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Текущий рздел конфигурации не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public void SetValue(string name, Size value) => Configuration.SetValue(Path, name, value);

        /// <summary>
        /// Создает или заменяет уже существующий праметр в разделе, представленным текущим <see cref="KeyInfo"/>.
        /// </summary>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="value">Устанавливаемое значение конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для объекта конфигурации <see cref="Configuration"/> были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="name"/> не может быть null. -или- Значение аргумента <paramref name="value"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="NotEnoughFreeSpaceException">Недостаточно свободных блоков для сохранения параметра конфигурации (Смотри так же <seealso cref="Config.FreeBlocksCount"/>).</exception>
        /// <exception cref="KeyNotFoundException">Текущий рздел конфигурации не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public void SetValue(string name, Version value) => Configuration.SetValue(Path, name, value);

        /// <summary>
        /// Переименовывает параметр конфигурации в разделе, представленным текущим <see cref="KeyInfo"/>.
        /// </summary>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="newName">Новое наименование параметра конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для объекта конфигурации <see cref="Configuration"/> были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="name"/> не может быть null. -или- Значение аргумента <paramref name="newName"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="name"/> содержит недопустимое значение. -или- Аргумент <paramref name="newName"/> содержит недопустимое значение.</exception>
        /// <exception cref="KeyNotFoundException">Текущий рздел конфигурации не существует.</exception>
        /// <exception cref="ValueNotFoundException">Раздел конфигурации <paramref name="name"/> не существует в текщем разделе.</exception>
        /// <exception cref="ValueAlreadyExistException">Раздел конфигурации с именем <paramref name="newName"/> уже присутствует в текущем разделе.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public void RenameValue(string name, string newName) => Configuration.RenameValue(Path, name, newName);

        /// <summary>
        /// Возвращает параметр конфигурации в разделе, представленным текущим <see cref="KeyInfo"/>.
        /// </summary>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <returns>Объект <see cref="ValueInfo"/>, описывающий параметр конфигурации.</returns>
        /// <exception cref="ObjectDisposedException">Все ресурсы для объекта конфигурации <see cref="Configuration"/> были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="KeyNotFoundException">Текущий рздел конфигурации не существует.</exception>
        /// <exception cref="ValueNotFoundException">Раздел конфигурации <paramref name="name"/> не существует в текщем разделе.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public ValueInfo GetValueInfo(string name) => Configuration.GetValueInfo(Path, name);

        /// <summary>
        /// Получает значение параметра конфигурации с указанием значения по умолчанию и необходимостью проверять тип параметра конфигурации в разделе, представленным текущим <see cref="KeyInfo"/>.
        /// </summary>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="defaultValue">Значение по умолчанию.</param>
        /// <param name="checkType">Значение true, если тип данных значение по умолчанию <paramref name="defaultValue"/> не будет совпадать с типом данных параметра, то будет возвращено значение по умолчанию <paramref name="defaultValue"/>; если значение false, значение по умолчанию <paramref name="defaultValue"/> будет возвращено только если параметр отсутствует.</param>
        /// <returns>Значение параметра или значение по умолчанию <paramref name="defaultValue"/> если параметр отсутствует или его тип не совпадает с ожидаемым при включённгой проверке типов <paramref name="checkType"/></returns>
        /// <exception cref="ObjectDisposedException">Все ресурсы для объекта конфигурации <see cref="Configuration"/> были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="name"/> не может быть null. -или- Значение аргумента <paramref name="defaultValue"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public object GetValue(string name, object defaultValue, bool checkType) => Configuration.GetValue(Path, name, defaultValue, checkType);

        /// <summary>
        /// Получение значения параметра конфигурации с указанием значения по умолчанию и проверкой типа параметра конфигурации в разделе, представленным текущим <see cref="KeyInfo"/>.
        /// </summary>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="defaultValue">Значение по умолчанию.</param>
        /// <returns>Значение параметра или значение по умолчанию <paramref name="defaultValue"/> если параметр отсутствует или его тип не соответствует типу значения по умолчанию.</returns>
        /// <exception cref="ObjectDisposedException">Все ресурсы для объекта конфигурации <see cref="Configuration"/> были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="name"/> не может быть null. -или- Значение аргумента <paramref name="defaultValue"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public object GetValue(string name, object defaultValue) => Configuration.GetValue(Path, name, defaultValue, true);

        /// <summary>
        /// Получение значения параметра в разделе, представленным текущим <see cref="KeyInfo"/>.
        /// </summary>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <returns>Значение параметра конфигурации.</returns>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="KeyNotFoundException">Текущий рздел конфигурации не существует.</exception>
        /// <exception cref="ValueNotFoundException">Раздел конфигурации <paramref name="name"/> не существует в текщем разделе.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public object GetValue(string name) => Configuration.GetValue(Path, name);

        /// <summary>
        /// Получение значения параметра как строки в разделе, представленным текущим <see cref="KeyInfo"/>.
        /// </summary>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="defaultValue">Значение по умолчанию.</param>
        /// <param name="canConvertType">Значение true, если нужно преобразовать значение другого типа к строке; значение false — будет возвращено значение по умолчанию <paramref name="defaultValue"/>, для несоответствующего типа параметра.</param>
        /// <returns>Значение параметра или значение по умолчанию <paramref name="defaultValue"/> если прараметр отсутствует или имеет не строковый тип с отключённой конвертацией <paramref name="canConvertType"/></returns>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public string GetValueAsString(string name, string defaultValue = null, bool canConvertType = true) => Configuration.GetValueAsString(Path, name, defaultValue, canConvertType);

        /// <summary>
        /// Получение значения параметра как логическое знчаение в разделе, представленным текущим <see cref="KeyInfo"/>.
        /// </summary>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="defaultValue">Значение по умолчанию.</param>
        /// <param name="canConvertType">Значение true, если нужно преобразовать значение другого типа к строке; значение false — будет возвращено значение по умолчанию <paramref name="defaultValue"/>, для несоответствующего типа параметра.</param>
        /// <returns>Значение параметра или значение по умолчанию <paramref name="defaultValue"/> если прараметр отсутствует или имеет не строковый тип с отключённой конвертацией <paramref name="canConvertType"/></returns>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public bool GetValueAsBool(string name, bool defaultValue = false, bool canConvertType = true) => Configuration.GetValueAsBool(Path, name, defaultValue, canConvertType);

        /// <summary>
        /// Получение значения параметра как <see cref="Guid"/> в разделе, представленным текущим <see cref="KeyInfo"/>.
        /// </summary>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="defaultValue">Значение по умолчанию.</param>
        /// <param name="canConvertType">Значение true, если нужно преобразовать значение другого типа к строке; значение false — будет возвращено значение по умолчанию <paramref name="defaultValue"/>, для несоответствующего типа параметра.</param>
        /// <returns>Значение параметра или значение по умолчанию <paramref name="defaultValue"/> если прараметр отсутствует или имеет не строковый тип с отключённой конвертацией <paramref name="canConvertType"/></returns>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public Guid GetValueAsGuid(string name, Guid defaultValue = default, bool canConvertType = true) => Configuration.GetValueAsGuid(Path, name, defaultValue, canConvertType);

        /// <summary>
        /// Получение значения параметра как <see cref="Version"/> в разделе, представленным текущим <see cref="KeyInfo"/>.
        /// </summary>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="defaultValue">Значение по умолчанию</param>
        /// <param name="canConvertType">Значение true, если нужно преобразовать значение другого типа к строке; значение false — будет возвращено значение по умолчанию <paramref name="defaultValue"/>, для несоответствующего типа параметра.</param>
        /// <returns>Значение параметра или значение по умолчанию <paramref name="defaultValue"/> если прараметр отсутствует или имеет не строковый тип с отключённой конвертацией <paramref name="canConvertType"/></returns>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public Version GetValueAsVersion(string name, Version defaultValue = null, bool canConvertType = true) => Configuration.GetValueAsVersion(Path, name, defaultValue, canConvertType);

        /// <summary>
        /// Получение значения параметра конфигурации с указанным типом значения в разделе, представленным текущим <see cref="KeyInfo"/>.
        /// </summary>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <param name="defaultValue">Значение по умолчанию.</param>
        /// <returns>Значение параметра или значение по умолчанию <paramref name="defaultValue"/> если параметр отсутствует или его тип не соответствует типу значения по умолчанию.</returns>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public TValue GetValueAs<TValue>(string name, TValue defaultValue = default) => Configuration.GetValueAs<TValue>(Path, name, defaultValue);

        /// <summary>
        /// Удаляет параметр конфигурации из раздела, представленного текущим <see cref="KeyInfo"/>.
        /// </summary>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <exception cref="ObjectDisposedException">Все ресурсы для текущего объекта конфигурации были освобождены.</exception>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="KeyNotFoundException">Текущий рздел конфигурации не существует.</exception>
        /// <exception cref="ValueNotFoundException">Раздел конфигурации <paramref name="name"/> не существует в текщем разделе.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public void DeleteValue(string name) => Configuration.DeleteValue(Path, name);

        /// <summary>
        /// Выполняет проверку наличия параметра конфигурации в разделе, представленным текущим <see cref="KeyInfo"/>
        /// </summary>
        /// <param name="name">Наименвоание параметра конфигурации.</param>
        /// <returns>Значение true, если параметр конфигурации <paramref name="name"/> существует в разделе конфигурации; в противном случае — значение false.</returns>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="name"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="name"/> содержит недопустимое значение.</exception>
        /// <exception cref="KeyNotFoundException">Текущий рздел конфигурации не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public bool ExistValue(string name) => Configuration.ExistValue(Path, name);

        #endregion

        #region Разное

        /// <summary>
        /// Возвращает строку, представляющую текущий объект.
        /// </summary>
        /// <returns>Путь к разделку конфигурации, представленным текщим объектом <see cref="KeyInfo"/>.</returns>
        /// <remarks>Соответствует свойству <see cref="Path"/></remarks>
        public override string ToString() => Path;

        /// <summary>
        /// Возвращает хеш-код для этого <see cref="KeyInfo"/>
        /// </summary>
        /// <returns>Хеш-код для текущего <see cref="KeyInfo"/></returns>
        public override int GetHashCode() => Path.GetHashCode();

        /// <summary>
        /// Определяет, равен ли заданный объект текущему <see cref="KeyInfo"/>.
        /// </summary>
        /// <param name="obj">Объект, который требуется сравнить с текущим <see cref="KeyInfo"/>.</param>
        /// <returns>Значение true, если заданный объект и текущий <see cref="KeyInfo"/> равны; иначе — значение false.</returns>
        public override bool Equals(object obj)
        {
            if (obj != null) {
                if (obj is KeyInfo) return Equals((KeyInfo)obj);
                else if (obj is string) return Equals((string)obj);
            }
            return false;
        }

        /// <summary>
        /// Определяет, равен ли заданный объект <see cref="KeyInfo"/> текущему <see cref="KeyInfo"/>.
        /// </summary>
        /// <param name="keyInfo">Объект <see cref="KeyInfo"/>, который требуется сравнить с текущим <see cref="KeyInfo"/>.</param>
        /// <returns>Значение true, если заданный объект <see cref="KeyInfo"/> и текущий <see cref="KeyInfo"/> равны; иначе - значение false.</returns>
        public bool Equals(KeyInfo keyInfo) => keyInfo != null && Registry == keyInfo.Registry && Configuration == keyInfo.Configuration && Equals(keyInfo.Path);

        /// <summary>
        /// Определяет, соответствует ли путь к раздел конфигурации, представленный текущим объектом <see cref="KeyInfo"/>, заданной строке.
        /// </summary>
        /// <param name="str">Строка, которую нужно сравнить с путём к разделу конфигурации, представленному  текущим объектом <see cref="KeyInfo"/>.</param>
        /// <returns>Значение true, если путь к разделу конфигурации, представленный текущим объектом <see cref="KeyInfo"/>, соответствует заданной строке; иначе — значение false.</returns>
        public bool Equals(string str) => string.Equals(Path, str, StringComparison.CurrentCultureIgnoreCase);

        int IComparable<KeyInfo>.CompareTo(KeyInfo keyInfo)
        {
            if (keyInfo == null) return 1;
            var result = (DepthOfNesting != keyInfo.DepthOfNesting) ? ((DepthOfNesting < keyInfo.DepthOfNesting) ? -1 : 1) : 0;
            if (result == 0) result = string.Compare(Path, keyInfo.Path, true, CultureInfo.CurrentCulture);
            return result;
        }

        int IComparable<string>.CompareTo(string str) => string.Compare(Path, str, true, CultureInfo.CurrentCulture);

        #endregion

        #region Операторы

        /// <summary>
        /// Создаёт объект <see cref="KeyInfo"/> для указанного подраздела
        /// </summary>
        /// <param name="keyInfo">Объект <see cref="KeyInfo"/>, представляющий раздел, подраздел которого нужно открыть</param>
        /// <param name="path">Относительный путь к подразделу</param>
        /// <returns>Объект <see cref="KeyInfo"/>, описывающий указанный в <paramref name="path"/> раздел</returns>
        /// <exception cref="ObjectDisposedException">Для конфигурации, к которой привязан данный раздел (свойство <see cref="Configuration"/>) был вызван метод <see cref="Config.Dispose()"/></exception>
        /// <exception cref="ArgumentException">Путь к разделу конфигурации <paramref name="path"/> не задан, имеет недопустимый формат или превышает глубину вложенности</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода</exception>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Usage", "CA2225", Justification = "<Ожидание>")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1062:", Justification = "<Ожидание>")]
        public static KeyInfo operator +(KeyInfo keyInfo, string path) => keyInfo.OpenSubKey(path);

        #endregion

        #endregion

        #region Свойства

        /// <summary>
        /// Возвращает объект <see cref="Config"/>, представляющий конфигурацию, частью которой является текущий раздел.
        /// </summary>
        public Config Configuration { get; }

        /// <summary>
        /// Реестр конфигурации, в который входит текущий раздел
        /// </summary>
        /// <returns>Объект <see cref="Registry"/>, представляющий реестра конфигурации, частью которого является текущий раздел конфигурации</returns>
        public Registry Registry => Configuration.Registry;

        /// <summary>
        /// Возвращает путь к разделу конфигурации в пределах объекта конфигурации <see cref="Configuration"/>.
        /// </summary>
        public string Path { get; }

        /// <summary>
        /// Возвращает полный путь к разделу конфигурации с учётом свойства <see cref="Registry"/>.
        /// </summary>
        public string FullPath => (Registry?.GetRegistryKeyName(Configuration) ?? string.Empty) + Path;

        /// <summary>
        /// Возвращает значение true, если текущий раздел конфигурации существует; в противном случае — значение false.
        /// </summary>
        /// <exception cref="ObjectDisposedException">Все ресурсы для объекта конфигурации <see cref="Configuration"/> были освобождены.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public bool Exist => Configuration.ExistKey(Path);

        /// <summary>
        /// Получает список имен подразделов входящих в текущий раздел.
        /// </summary>
        /// <exception cref="ObjectDisposedException">Все ресурсы для объекта конфигурации <see cref="Configuration"/> были освобождены.</exception>
        /// <exception cref="KeyNotFoundException">Текущий рздел конфигурации не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public IReadOnlyList<string> NameKeys => Configuration.GetNameKeys(Path);

        /// <summary>
        /// Получает массив объектов <see cref="KeyInfo"/>, представляющих подразделы для текущего раздела.
        /// </summary>
        /// <exception cref="ObjectDisposedException">Все ресурсы для объекта конфигурации <see cref="Configuration"/> были освобождены.</exception>
        /// <exception cref="KeyNotFoundException">Текущий рздел конфигурации не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public IReadOnlyList<KeyInfo> Keys => NameValues.Select(keyname => OpenSubKey(keyname)).ToArray();

        /// <summary>
        /// Получает список имен параметров входящих в текущий раздел.
        /// </summary>
        /// <exception cref="ObjectDisposedException">Все ресурсы для объекта конфигурации <see cref="Configuration"/> были освобождены.</exception>
        /// <exception cref="KeyNotFoundException">Текущий рздел конфигурации не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public IReadOnlyList<string> NameValues => Configuration.GetNameValues(Path);

        /// <summary>
        /// Получает список параметров входящих в текущий раздел.
        /// </summary>
        /// <exception cref="ObjectDisposedException">Все ресурсы для объекта конфигурации <see cref="Configuration"/> были освобождены.</exception>
        /// <exception cref="KeyNotFoundException">Текущий рздел конфигурации не существует.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        public IReadOnlyList<ValueInfo> InfoValues => Configuration.GetInfoValues(Path);

        /// <summary>
        /// Получает глубину вложенности текущего раздела
        /// </summary>
        public int DepthOfNesting => (Path != "/") ? Path.Trim('/').Split('/').Length : 0;

        /// <summary>
        /// Возвращает значение true, если текущий раздел конфигурации имеет максимальную глубину вложености; в противном случае — значение false.
        /// </summary>
        public bool IsMaximumNestingDepth => DepthOfNesting >= Configuration.MaximumNestingDepth;

        /// <summary>
        /// Возвращает значение true, если раздел, описанный теущим <see cref="KeyInfo"/>, является корневым.
        /// </summary>
        public bool IsRoot => Path == "/";

        /// <summary>
        /// Возвращает путь к родительскому разделу конфигурации или пустую строку, если текущий раздел является корневым.
        /// </summary>
        public string ParentPath
        {
            get {
                var keynames = Path.Trim('/').Split('/');
                if (keynames.Length > 1 || (keynames.Length == 1 && keynames[0].Length != 0)) return "/" + string.Join("/", keynames, 0, keynames.Length - 1);
                return string.Empty;
            }
        }

        /// <summary>
        /// Возвращает путь к родительскому разделу конфигурации с учётом свойства <see cref="Registry"/> или пустую строку, если текущий раздел является корневым.
        /// </summary>
        public string FullParentPath {
            get {
                var parentpath = ParentPath;
                return !string.IsNullOrEmpty(parentpath) ? (Registry?.GetRegistryKeyName(Configuration) ?? string.Empty) + parentpath : string.Empty;
            }
        }

        /// <summary>
        /// Объект <see cref="KeyInfo"/>, представляющий родительский раздел, или значение null, если текущий раздел является корневым.
        /// </summary>
        public KeyInfo Parent
        {
            get {
                var keynames = Path.Trim('/').Split('/');
                if (keynames.Length > 1 || (keynames.Length == 1 && keynames[0].Length != 0)) {
                    var parentpath = "/" + string.Join("/", keynames, 0, keynames.Length - 1);
                    return new KeyInfo(Configuration, parentpath);
                }
                return null;
            }
        }

        #endregion

    }

}
